all: mkdocs

mkdocs: mkdocs.yml
	/usr/local/bin/mkdocs build

gitbook:
	@gitbook build;\
	if [ $$? -gt 0 ]; then\
		echo;\
		echo "!!!!!! Build failed, probably because of missing plugins, proceeding to INSTALL";\
		echo;\
		/usr/local/bin/gitbook install;\
		/usr/local/bin/gitbook build;\
	fi
	./correct_toc.sh _book
	./correct_urls_in_summary.sh
#	cp -rf ./include _book/gitbook/
#	rm -f _book/gitbook/style.css
#	cp -f ./include/css/isembench.css _book/gitbook/style.css
#	ln -s ../include/images/shredder.png _book/gitbook/images/shredder.png
#	./correct_title_href.sh _book

clean:
	rm -rf _book

cleanall: clean
	rm -rf node_modules
