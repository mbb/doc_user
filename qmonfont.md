# QMON

Sur ubuntu, il manque généralement des polices afin de pouvoir lancer qmon :

```
sudo apt-get install t1-xfree86-nonfree ttf-xfree86-nonfree ttf-xfree86-nonfree-syriac xfonts-75dpi xfonts-100dpi libfonts-java libxm4 xfstt libXext-dev libx11-dev libxpm-dev libxext-dev libfreetype6-dev libfontconfig1-dev x11proto-xext-dev gsfonts-x11 gsfonts-other ttf-unfonts-extra
sudo reboot
```
Si ça ne fonctionne toujours pas, vérifier la présence des paquets utiles importants : build-essential, binutils, make, gcc, g++, python-dev...