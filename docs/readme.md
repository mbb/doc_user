# Documentation de la plate-forme calcul et bioinformatique ISEM / MBB

> Pour regarder <a href="https://gitlab.mbb.cnrs.fr/doc_user/site/" target="_blank">en dehors du site calcul</a>

  - [Quoi utiliser](quoi_utiliser.md)
  - [FAQ](FAQ.md)

Documentations cluster ISEM :

  - [Démarrage sur le cluster ISEM](cluster/readme.md)
  - [Guix](guix.md)
  - [Mpi et Rmpi](mpi-rmpi.md)
  - [Singularity](singularity.md)
  - [R](r.md)
  - [Python](python.md)
  - [Perl](perl.md)
  - [C++ et MPI](cpp.md)
  - [java](java.md)
  - [Gnuplot](gnuplot.md)

  - [Ancien tutoriel de démarrage MBB](tutombb.md)

Autres documentations :

  - [Gitlab](gitlab.md)
  - [Les bigmems](bigmems/readme.md)
    - [Description du service](bigmems/bigmems.md)
    - [Prise en main d'une machine bigmem](bigmems/post-configuration.md)
    - [Exemple de sauvegarde des bigmems](bigmems/backup.md)
    - [Notions de sécurisation et mises à jour](bigmems/updates_and_security.md)
  - [Les alternatives à l'extérieur de l'ISEM](alternatives_exterieures.md)