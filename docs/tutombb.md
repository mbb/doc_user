# Toutes les questions que vous vous posez sur MBB

<!-- toc -->

# Introduction

[MBB platform](http://mbb.univ-montp2.fr)

MBB est une plateforme de calcul et Web qui permet de lancer des jobs (soit des logiciels directement en ligne depuis un navigateur, soit avec un accès direct en SSH). Le but est de promouvoir les logiciels développés au sein des laboratoires du [LabEx CeMEB](http://www.labex-cemeb.org/) mais aussi de donner aux personnels de ces laboratoires une puissance de calcul d'appoint dont ils ne disposent pas forcément. Cette plateforme mutualisée est hébergée physiquement au sein de [l'ISEM](http://www.isem.univ-montp2.fr/) par l'équipe [calcul](http://cbi.isem.univ-montp2.fr).

L'ouverture d'un compte est donc limitée aux personnels affiliés à l'un des instituts/laboratoires du LabEx CeMEB.

Cette page décrit l'accès et l'utilisation de deux services :
  - le **cluster MBB en ligne de commande par SSH** pour soumission de jobs par SGE
  - le service **Web MBB** (MBB Web : https://mbb.univ-montp2.fr) qui permet de soumettre des jobs en ligne depuis une interface Web mais qui est plus limité que le service ci-dessus.

# Comment ouvrir un compte pour un acces en ligne à MBB Web

Si vous êtes membre du LabEx, commencez par vérifier si vous avez un [correspondant](http://mbb.univ-montp2.fr/MBB/subsection/Help.php#h_fac_15) pour la plateforme MBB. Ce dernier pourra vous créer directement un compte (si ce n'est pas le cas, ce dernier prendra contact avec nous pour que nous lui expliquions comment faire et, le cas échéant, lui rajouter les droits nécessaires).

Vous pouvez aussi faire la demande directement [ici](http://kimura.univ-montp2.fr/aide/index.php?a=add). Pour la catégorie, choisir _"Account creation for MBB (Website)"_ et n'oubliez pas de préciser votre laboratoire d'origine et votre équipe.

Lorsque vous aurez un compte, vous pourrez alors vous connecter dans l'interface Web de MBB, en haut à droite, par "Login".

# Comment ouvrir un compte sur MBB pour un acces direct par SSH

Les connexions SSH sont un peu plus limitées, mais la demande de création d'un compte se fait de la même manière. Vous devez vous rendre [ici](http://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html). Précisez la catégorie _"Account creation for MBB (SSH)"_. Dans le texte, indiquez quel est votre laboratoire et votre équipe. Un texte motivé est obligatoire.

## Acces reseau

Plusieurs possibilités s'offrent à vous:

- vous êtes physiquement dans un laboratoire du LabEx CeMEB. Voir avec votre [correspondant](http://mbb.univ-montp2.fr/MBB/subsection/Help.php#h_fac_15) de laboratoire.
- vous essayez de vous connecter de chez vous:
  - vous avec un compte à la faculté des Sciences; vous pouvez alors [utiliser le VPN](https://wikidocs.umontpellier.fr/pages/viewpage.action?pageId=1048647).
  - vous n'avez pas de compte à la Faculté des Sciences, mais vous avez une [adresse IP publique].

### Comment utiliser le VPN de la Faculté des Sciences de Montpellier

Si vous essayez de vous connecter depuis chez vous, le plus simple consiste à utiliser le VPN de l'Université de Montpellier (Pour utiliser le VPN de l'Université de Montpellier, il vous suffit d'avoir un compte à l'Université de Montpellier):
- [Lien VPN](https://wikidocs.umontpellier.fr/pages/viewpage.action?pageId=1048647)

Si vous n'avez pas de compte mail à l'UM, essayez d'utiliser un autre VPN ou bien :
  - faites une demande procédure spéciale de travail à distance en créant un ticket sur notre plateforme : http://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html
  - vérifiez que votre adresse est fixe/dynamique auprès de votre FAI (ou bien retournez régulièrement sur le site vous donnant votre adresse IP publique (voir le début de ce paragraphe) et vérifiez qu'elle reste la même (débranchez le réseau puis rebranchez-vous/redémarrez votre ordinateur...)),

### Comment connaître son adresse IP publique

Une adresse IP publique fixe est nécessaire pour les accès de type SSH à la plateforme depuis l'extérieur de la [Faculté des Sciences de Montpellier](http://www.fdsweb.univ-montp2.fr/). Les adresses IP fournies par les fournisseurs d'accès à Internet sont souvent des adresses IP dynamiques.

Cette adresse doit être communiquée avec la demande de création de compte SSH.

Pour connaître la votre, le plus simple est de taper dans google _"what is my IP"_; vous tomberez alors sur de nombreux sites pouvant vous indiquer quelle est votre adresse IP publique du point de vue du protocole HTTP (pour un accès SSH, il se peut que cette adresse ne soit pas la même, mais c'est rarement le cas).

### Comment se connecter par SSH selon votre système d'exploitation (cluster MBB en SSH)

- Sur Linux, rien de plus simple : ouvrez un terminal, puis lancez :

```bash
ssh nom_d_utilisateur@adresse_IP_du_cluster
```

En remplaçant _nom\_d\_utilisateur_ par votre nom d'utilisateur et _adresse\_IP\_du\_cluster_ par l'adresse qui vous sera indiquée lors de votre demande de compte). Si le client SSH n'est pas installé, le paquet se nomme [OpenSSH](http://www.openssh.com/).
- Sur MacOS vous pouvez faire la même chose. Le client est normalement installé en standard.

> **Note** Dans les deux cas, à votre première connexion, on vous demandera de rajouter une clé RSA/DSA. C'est tout à fait normal et vous devez l'accepter (en mettant "Y" pour Yes, par exemple...). Puis il vous faudra taper votre de mot de passe.

- Sur Windows, il vous faudra télécharger un client SSH, comme par exemple [Putty](http://www.putty.org), [MobaXterm](https://mobaxterm.mobatek.net/) ou [Tera Term](https://en.osdn.jp/projects/ttssh2/releases/). Sur ces logiciels, il vous faudra rentrer votre nom d'utilisateur et l'adresse du cluster. Choisissez SSH (le port par défaut est "22"), acceptez la clé puis tapez votre mot de passe.

# Je suis connecté en SSH, mais je ne sais pas quoi ou comment faire maintenant

Le système d'exploitation sur lequel vous êtes désormais connecté est un [linux](http://fr.wikipedia.org/wiki/Linux) 64 bits du type [Ubuntu](https://ubuntu.com/) (dérivé de [debian](https://www.debian.org/)).

Vous vous êtes connecté sur le cluster et vous êtes maintenant dans votre espace personnel nommé _Home Directory_.

Pour les commandes de base, vous trouverez énormément de sites sur Internet qui parlent du bash linux ([voir notre article](linux_base.md)). Une fois que vous savez naviguer dans les répertoires (cd), en créer (mkdir), en déplacer (mv), supprimer des fichiers ou des répertoires (rm), éditer des fichier (vi, nano...), lister le contenu des répertoires (ls), afficher le manuel ou l'aide d'une commande (man _ma_commande_, _ma_commande_ --help (-h, -help)) vous connaîtrez les commandes les plus utiles/utilisées.

- [une description de l'arborescence du système sous Linux sur commentcamarche.net](http://www.commentcamarche.net/contents/linux/linarb.php3)

# Comment connaître la liste des logiciels installés

## Liste des logiciels installés sur le cluster (connexion ssh)

Merci de vous référer à [cette documentation](cluster.md#cluster-logiciels-installes).

En cas de doute, vous pouvez nous poser la question en créant un ticket : http://kimura.univ-montp2.fr/aide/index.php?a=add

## Liste des logiciels installés sur MBB Web

En ligne, on va parler de _"Online Tools"_. Il s'agit en fait de _"wrapper"_ qui vont pouvoir lancer des logiciels sur le cluster MBB. Pour avoir la liste des logiciels _"wrappés"_, il vous suffit d'aller dans la catégorie Online Tools, de la section voulue, et de vérifier que le logiciel est dans la liste. Sinon, vous les avez tous [ici](http://mbb.univ-montp2.fr/MBB/subsection/onlineTools.php?section=all)

# Comment installer un logiciel

## Sur le cluster MBB par SSH

Merci de lire [ce document](cluster.md#install-cluster-software)

## Pour un accès en ligne avec MBB Web

2 cas :

- vous êtes développeur du logiciel: vous devez d'abord déterminer le niveau d'ouverture/de partage que vous voulez pour ce dernier, ainsi que le type d'actions réalisables au travers du site. En effet, peut-être désirez-vous uniquement que les sources soit disponibles en téléchargement (?) ou bien préférez-vous que le logiciel puisse être exécuté en ligne depuis un formulaire de soumission comme les autres Online Tools (avec un nombre d'options limitées ?). Peut-être avez-vous déjà réfléchi au cadre d'utilisation de votre logiciel et à la licence pour ce dernier ?
- vous n'êtes pas développeur du logiciel mais vous souhaitez utiliser un logiciel bioinformatique sous Linux, directement en ligne dans un formulaire: vous devez vous renseigner sur le type de licence.

Dans les deux cas, vous devez en faire la demande auprès de l'équipe de la plateforme, toujours à la même adresse:
http://kimura.univ-montp2.fr/aide/index.php?a=add
Dans la catégorie, choisissez "MBB Website OnLineTools)", et dans le corps, précisez un maximum d'informations (voir les questions ci-dessus).

# En SSH sur le cluster, comment utiliser SGE - et à quoi ça sert

Tout programme doit être lancé par le gestionnaire de jobs installé sur le cluster _SGE_.

SGE permet de contrôler les jobs, avec un système de priorité et de contrôle des ressources disponibles.

Vous pouvez commencer par lire ces quelques articles:
- [Utilisation basique de SGE](cluster.md#utilisation-basique-de-sge-et-explication-des-options)
- [How To SGE](cluster.md#how-to-sge)
- Vous pouvez aussi vous reportez à la [documentation de OpenGrid Scheduler](http://gridscheduler.sourceforge.net/howto/basic_usage.html)

# Comment transférer des donnees vers/depuis MBB

## Au travers d un accès au Cluster en SSH

Le plus simple consiste à utiliser les commandes directes scp ou sftp depuis un terminal en ligne de commande.

Si vous n'êtes pas très à l'aise avec l'utilisation des deux protocoles précédents, vous pouvez alors utiliser un client graphique comme filezilla: http://sourceforge.net/projects/filezilla/files/FileZilla_Client/

Dans ce cas, pensez à bien indiquer le protocole (par défaut, le protocole est FTP, mais ce dernier n'est pas disponible sur le cluster).

- Si vous êtes sous MacOS, vous pouvez aussi opter pour _cyberduck_.
- Si vous êtes sous Windows, _WinSCP_ est également très répandu (SCP/SFTP en protocole par défaut),
- Sinon vous avez aussi _gFTP_ sous Linux (là aussi, il faudra pensez à bien indiquer le protocole).

Vous pouvez ainsi vous connecter depuis ces clients (ou un autre client SCP/SFTP graphique non listé) sur le cluster avec votre nom d'utilisateur, votre mot de passe et vous devrez accepter la clé RSA/DSA du serveur.

### Cas particulier du transfert de données volumineuses

Dans le cadre du service cluster MBB en ligne de commande par SSH, il peut arriver que vous ayez besoin de transférer tout un gros projet ou un grand nombre de petits fichiers dans votre home directory ou vers un nas d'équipe. Dans ce cas, il est pertinent de transférer directement vers votre système de stockage final, sans passer par le noeud maître.

Pour faire cela, il faut au préalable récupérer l'adresse IP publique du nas puis il faudra [créer un ticket](http://cbi.isem.univ-montp2.fr/calcul/helpdesk_NewTicket.html) pour autoriser l'accès de votre machine vers le NAS.

Exemple avec un home directory :

```bash
# récupération de l'adresse IP et dossier du NAS de votre home
nas=$(mount |grep $USER |awk -F: '{print $1}')
dir=$(mount |grep $USER |awk -F'[: ]' '{print $2}')
# on affiche le nas et le dossier local sur le NAS correspondant à notre home directory
echo $nas
echo $dir

# ensuite pour avoir l'adresse IP
ssh $nas /sbin/ifconfig | awk -F'[: ]' '/162.38/ {print $13}'
```

## Au travers de l accès en ligne MBB web

Allez dans "Data", en haut, ou sur "account" > "My Storage Space" puis "Add new File". Pour récupérer des données, il suffit de cliquer dessus ou sur le lien "download" associé.
