# Dessiner des graphes à partir d'un fichier tabulaire sous gnuplot

Prenons un fichier tabulaire du type :
```
#Date Time Vitesse Propagation Accélération Phase
20110620 14:35:55 9 3 5 45
20110620 14:36:55 9 5 7 45
20110620 14:37:55 10 8 10 45
```

En abscisse, nous souhaitons avoir l'heure (colonne 2) et en ordonnées, les différentes valeurs. La ligne 1 représentera l'évolution de la vitesse (collone 3) en fonction du temps, la ligne 2 sera une fonction propagation par rapport au temps (colonne 4) etc...

Dans notre cas, le séparateur est celui par défaut sur gnuplot, il n'y a donc pas besoin de préciser un caractère particulier de séparation des colonnes. Si nous avions eu une virgule, il faudrait spécifier à gnuplot
```
gnuplot> set datafile separator ","
```
L'heure ici est au bon format (en entrée) pour gnuplot mais la date ne nous intéresse pas, il suffit donc de lui préciser le format de sortie de l'heure et indiquer que l'abscisse correspond à l'heure :
```
set xdata time
set timefmt "%H:%M:%S"
```
```
plot 'myfile.csv' using 2:3 with linespoint title 'vitesse',\
'myfile.csv' using 2:4 with linespoint title 'propagation',\
'myfile.csv' using 2:5 with linespoint title 'Acceleration'
```
Nous demandons à gnuplot de tracer l'évolution de nos variables en fonction de la colonne 2 en abscisse (le temps) en précisant un titre par courbe avec un tracé au format "linespoint" (ligne avec des points; il y a également lines, points...). Cela pourrait suffire à nos besoins. Pour aller plus loin, on peut indiquer à gnuplot un ensemble de colonnes à prendre en charge avec "every ::3::5" (toutes les colonnes de la 3ème à la 5ème).

<table>
<tbody>
<tr>
<th>Paramètre</th>
<th>Description</th>
</tr>
<tr>
<td>i</td>
<td>incrément de ligne</td>
</tr>
<tr>
<td>j</td>
<td>incrément de bloc</td>
</tr>
<tr>
<td>k</td>
<td>la première ligne</td>
</tr>
<tr>
<td>l</td>
<td>le premier bloc</td>
</tr>
<tr>
<td>m</td>
<td>la dernière ligne</td>
</tr>
<tr>
<td>n</td>
<td>le dernier bloc</td>
</tr>
</tbody>
</table>

Paramètres de l'option every i:j:k:l\:m:n

Parfois, il peut s'avérer nécessaire de fixer des limites aux axes (pas ici), par exemple :
```
set xrange ["14:35:00":"14:38:00"]
set yrange [0:10]
```
Nous pouvons également dire à gnuplot de ne pas afficher le graphique sur la sortie standard mais dans un fichier image de type png :
```
set terminal "png"
set output "myfile.png"
```
Nous pouvons aussi rajouter des titres aux axes et à notre graphique :
```
set title "Mon onde"
set xlabel "Time [HH:MM:SS]"
set ylabel "Y values"
```


Code Final gnuplot
```

set terminal "png"
set output "myfile.png"
set xlabel "Time [HH:MM:SS]"
set ylabel "Y values"
set xdata time
set timefmt "%H:%M:%S"
plot 'myfile.csv' using 2:3 with linespoint title 'vitesse',\
'myfile.csv' using 2:4 with linespoint title 'propagation',\
'myfile.csv' using 2:5 with linespoint title 'Acceleration'

```

![myfile](myfile1.png)
