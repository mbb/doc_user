<!-- toc -->

# Prise en main rapide du cluster de calcul

## Demarrer sur le cluster

### Linux

Des bases sous Linux sont requises pur utiliser un cluster de calcul. Le minimum étant de savoir comment lancer son logiciel. Cependant, sur un cluster de calcul, on ne va pas lancer un logiciel comme on ferait sur sa machine personnelle.

En effet, en premier lieu, il y a rarement une interface graphique disponible (bien que parfois possible sur certains noeuds de visualisation, mais ce n'est pas le cas sur le cluster l'ISEM). Ensuite, parce que le but est d'utiliser un gestionnaire de job ou `Job Scheduler`, qui va s'occuper de gérer au mieux les ressources disponibles, et, en fonction de vos droits, accès et de la charge actuelle, vous attribuer des ressources dans lesquelles vos jobs et tâches seront exécutées.

Dans [SLURM](https://slurm.schedmd.com/), le gestionnaire de job utilisé sur le cluster `BlueBanquise`, les tâches sont vues comme des sous-ensembles d'étapes dont l'ensemble forme un "job".

* [Définition wikipedia du Job Scheduler](https://en.wikipedia.org/wiki/Job_scheduler),
* Pour plus d'information sur le découpage entre `step`, `tasks` et `jobs` dans SLURM : [https://stackoverflow.com/a/46532581](https://stackoverflow.com/a/46532581).


> **Pour consulter la documentation actuelle, [cliquez ici](cluster/readme.md).**

### Pour ceux qui viennent de SGE

Vous pouvez regarder [ce guide conversion des commandes proposé par le Max Planck Computing & Data Facility](https://docs.mpcdf.mpg.de/doc/computing/clusters/aux/migration-from-sge-to-slurm).

> La suite de ce guide ne s'applique plus sur le cluster ISEM qui fonctionne sous [SLURM](https://slurm.schedmd.com/quickstart.html)


* Voici quelques commandes SGE (pour plus d'infos `man sge_intro` ou https://arc.liv.ac.uk/SGE/htmlman/manuals.html) :

 - `qsub` : submit a batch job to Grid Engine,
 - `qstat` : show the status of Grid Engine jobs and queues,
 - `qdel` : delete Grid Engine jobs from queues,
 - `qacct` : search in SGE history,
 - `qmon` : GUI front-end to user's and administrator's utilities.

* En general vous devez encapsuler la commande d'appel de votre executable dans un script ex.:

```bash
#! /bin/sh
# Ceci est un tres simple exemple de job sequentiel
# Options SGE:
# utilise /bin/sh:
#$ -S /bin/sh
#
# nom:
#$ -N mon_test_sge
#
# envoyer un mail à cet utilisateur
#$ -M belkhir univ-montp2.fr
# envoie de mail lorsque le job est fini
#$ -m e
#
# utilise le repertoire courant pour lancer le job test
# avec comme paramètre le fichier param1.txt
#$ -cwd

/home/khalid/bin/test /home/khalid/data/param1.txt
```

Pour soumettre ce script (monscript.sh) il faut :

```bash
qsub monscript.sh
```

* Une alternative est d’utliser l’option “-b y” pour signifier que vous présentez un binaire à la commande qsub :

```bash
qsub -b y /home/khalid/bin/test /home/khalid/data/param1.txt
```

* `qstat` et `qmon` (version graphique) vous permettent de suivre l’état de votre job.

# Utilisation basique de SGE et explication des options

## Introduction

GridEngine est un gestionnaire de ressources et un [job scheduler](http://en.wikipedia.org/wiki/Job_scheduler) (SGE, édité à l'origine par Sun et aujourd'hui remplacé de manière OpenSource par [OpenGrid Scheduler](http://gridscheduler.sourceforge.net/) sur nos clusters).

Ça signifie que GridEngine est capable de surveiller les ressources disponibles et d'ordonnancer les jobs afin de garantir une utilisation optimale de l'ensemble des ressources gérées pour tous les utilisateurs et d'exécuter les tâches demandées par ces derniers (jobs) en fonction des politiques implémentées par l'administrateur.

### Pour quels types de jobs

Comme indiqué sur [cette page](http://cc.in2p3.fr/docenligne/1007#0.1.2), GridEngine peut être utilisé pour des jobs "standards", c'est à dire séquentiels, ou des jobs peu ou fortement parallélisés (multithreads, tableaux de jobs, environnement parallèles (mpi...).

### Execution des jobs

Un job va être stocké (statut "qw" pour "queue wait job") dans une queue avant de passer en exécution (statut "r" pour "running job") en fonction des droits de l'utilisateur et des ressources disponibles. Si ce dernier demande une queue particulière, alors il pourra lancer des jobs dessus à condition d'y être autorisé; si les ressources sont utilisées alors ils seront en attente.
Une queue correspond normalement à un groupe de machines appelés "noeuds" de calcul.

## Lancer des jobs avec qsub

```bash
qsub
```

Pour _"Queue Submit"_ est la commande principale à connaître pour utiliser GridEngine.Vous devez insérer votre commande ou votre script à soumettre au travers d'un *script de soumission*.

Ce dernier, que nous allons appeler test.sge pour l'exemple, doit être articulé de la manière suivante :

```bash
#!/bin/bash

# Shell to use
#$ -S /bin/bash
# Name of the job in SGE
#$ -N my_job
# Name of the queue to use (optionnal)
#$ -q cemeb.q
# Maximum hardware time allowed for this job (required)
#$ -l h_rt=00:33:00
# Merge output (error and standard with '-j y') into one single file called my_job.out
#$ -o my_job.out -j y
# run in the current directory
#$ -cwd

YOUR_COMMAND_OR_SCRIPT_TO_EXECUTE
```

En bash, les commentaires commencent par « # ». Ainsi, les lignes d’options SGE ne sont pas lues par bash. Cependant, tout ce qui commence par « #$  » correspond à une option SGE, que ce dernier va ensuite interpréter. Chaque option SGE (prédéfinie par "#$") est suivie du nom de l'argument et de sa valeur. Les options peuvent également être directement appelées au moment du `qsub`.

Vous lancez ensuite ce script avec :

```bash
qsub test.sge
```

  - Intérêt de l'option -l `h_rt=HH:MM:SS`
  Cette option permet de faire tourner un job alors qu'une réservation de ressources sur le cluster (Advanced Reservation (AR)[\*](#nb) est déjà en place sur un cluster. Si h\_rt est inférieur à la durée restante avant que la réservation commence réellement il est alors possible que votre job soit soumis sur des ressources réservées. On appelle ça du backfilling.
  - `s_rt` fait la même chose que h\_rt mais envoie un signal de type SIGXUSR1 qui peut être intercepté par le programme si ce dernier en a la capacité.

> **Danger** L'options -l h\_rt (contrairement à s\_rt) est désormais *OBLIGATOIRE*. (sinon elle sera définie avec une valeur par défaut (`-l h_rt=12:00:00`)).

  - Une autre option très utile pour qsub est "-v" ou mieux, "-V". Avec cette option, vous pouvez lancer votre job et conserver une partie (-v suivi de la ou des variable(s) d'environnement) ou la totalité (-V) de vos variables d'environnement.
  - Il est également possible de lancer directement des jobs sans passer par un script, mais ce n'est pas recommandé (option "-b y").
  - Concernant un usage plus avancé (tableaux de jobs, synchronisation, mailing...), pour plus d'informations, merci d'utiliser la commande *man*

Pour plus d'informations :

```bash
man qsub
```

Pour un exemple plus complet, vous pouvez regarder le fichier general_qsub_file.sh dans votre dossier personnel ($HOME).

> **Note** A noter que le paramètre `-l s_rt=...`, un temps obligatoire, n'est plus nécessaire (au contraire du paramètre `-l h_rt=HHH:MM:SS`)

Un petit exemple fournit par rocks cluster / SGE : http://www.rocksclusters.org/roll-documentation/sge/4.2.1/submitting-batch-jobs.html

### Soumettre un job sur un noeud ou une queue precise

Choix de la queue longue :

```bash
qsub -q long.q myjob.sh
# ou
qsub -l qname=long.q myjob.sh
```

Choix du noeud 10 :

```bash
qsub -l hostname=compute-0-10 myjob.sh
```

## Afficher l etat des files d attente

```bash
qstat
```

Cette commande retourne plusieurs colonnes :
Le job id, suivi de sa priorité, son nom (ça serait my_job dans l'exemple), l'utilisateur qui a soumis le job, l'état, le moment où ça a été soumis, sur quelle queue ça s'exécute, le nombre de slots que ça prend et éventuellement l'id d'un job provenant d'un tableau de job.

```bash
qstat -u \*

#pour afficher uniquement vos jobs
qstat -u $USER

#pour compter mes jobs
qstat -u $USER | wc -l

#pour plus de détails
qstat -f

#pour une vue résumée de la charge des queues
qstat -g c

#uniquement les jobs qui tournent (état r)
qstat -sr

#cacher les queues vides
qstat -ne

#avoir tous les details sur le job n° 999
qstat -j 999
```

```bash
qstat -j JOB_ID;
```

Il y a plusieurs états :
  * "r" pour running (tourne)
  * "qw" pour queue wait (en attente)
  * "t" pour transfert
  * "dt" pour disable transfert (une erreur est survenue à la copie des informations du job)
  * "E" pour erreur ("qstat -explain E" pour afficher pourquoi)
  * ...

Vous pouvez combiner également plusieurs options pour qstat; exemple :

```bash
qstat -s r -l qname=cemeb.q,hostname=compute-0-5.local
```

Affiche les jobs en état "running" ("-s r" = state running (pour afficher ceux en attente, utiliser l'option "p" / pending)), sur les ressources de type "nom de la queue" = "cemeb.q" et "nom du noeud" = "compute-0-5.local".

L'option "-u" ne fonctionne pas chez nous car il y a un "alias" : alias qstat='qstat -u \*'". Ce qui permet, par défaut, de faire un qstat sur tous les utilisateurs (si vous souhaitez tout de même filtrer les sorties par cette option, il vous faudra entrer le chemin complet de la commande (retourné par "which qstat") ou vous pouvez avoir le résultat équivalent en filtrant le résultat avec un [grep](http://www.gnu.org/software/grep/manual/grep.html)).

Pour plus d'informations, sur [qstat](#afficher-l-etat-des-files-d-attente).

```bash
man qstat
```

## Supprimer un job avec qdel

*qdel* permet de supprimer un job. Son utilisation est assez simple.

```bash
qdel JOB_ID
```

Pour rappel, vous pouvez récupérer le JOB_ID avec qstat.

Voir [ici](cluster/cluster_gestion_des_jobs.md#gerer-ses-jobs-sous-slurm)

## Pour aller plus loin

Voir sur quelles machines tournent les jobs :

```bash
qhost -j
```

A quelles queues appartiennent les machines :

```bash
qhost -q
```

Voir également le [HOW TO](#how-to) et ce [document cirad](http://gohelle.cirad.fr/cluster/SGE-Utilisateur-v1.0.pdf).

## Determiner si vous avez accès à une queue

Lister les queues actives :

```bash
qconf -sql
```

Voir quelles sont les machines auxquelles vous avez accès dans cette queue :

```bash
qselect -q QUEUE -U $USER
```

Pour plus de détails voir [ici](#acces-aux-queues).

Avoir des informations sur une queue spécifique :

```bash
qconf -sq QUEUE
```

Vous pouvez Repérer la ligne "user_lists". S'il y a écrit "NONE", et que "xuser_lists" est également à NONE, alors tout le monde est autorisé à lancer des jobs sur cette queue.
Sinon, s'il y a autre chose, il faut que vous vérifiez que vous fassiez bien partie des "Userset" définis ici;

Exemple :

```bash
 qconf -su
```

Attention toutefois, vous pouvez aussi être associé à un Userset, non pas au travers de votre nom d'utilisateur, mais au travers de votre groupe d'appartenance.

Pour afficher des informations (uid, gid, groupes d'appartenance) sur votre compte :

```bash
id
```

<a id="NB"></a>
(\*) : pour voir s'il y a des réservations et afficher les détails d'une réservation:

Pour les détails des réservations :

```bash
qrstat -u \*
```

Pour le détail d'une réservation (avec son id récupéré au-dessus) :

```bash
qrstat -ar 103
```

<a id="cluster-logiciels-installes"></a>

# Quels logiciels sont installes sur le cluster

De nombreux logiciels sont déjà présents dans le dossier `/share/apps/bin`. Ce dossier est visible sur tous les noeuds de calcul et est le même sur les deux clusters (synchronisé toutes les nuits). La commande [module](#utiliser-module) permet également de charger certains logiciels.

Quand le logiciel n'est pas présent, nous demandons à faire un conteneur ([voir documentation Singularity](singularity.md)) (ou nous aidons à le faire / au pire, nous le faisons). En dernier recours, nous installons directement sur le noeud (dans /usr/local/bin ou /usr/local/[nom_du_logiciel] ou dans un autre chemin standard contenant des binaires).

Si l'application que vous souhaitez utiliser n'est pas disponible, plusieurs choix s'offrent à vous :
  1. Créer un conteneur singularity du programme,
  2. Demander de l'aide pour la création du conteneur [en faisant un ticket](http://kimura.univ-montp2.fr/aide/index.php?a=add),
  3. L'installer dans votre home quand c'est possible (à préférer). Votre répertoire personnel (home) est visible sur tous les noeuds,
  4. Demander son installation dans `/share/apps/bin` ou dans votre home ([en faisant un ticket](http://kimura.univ-montp2.fr/aide/index.php?a=add)) (à éviter dans la mesure du possible).

Dans la grande majorité des cas, vous n'aurez pas besoin de privilèges élevés pour installer et utilier un logiciel sur le cluster. Même ceux qui nécessitent un `make install` peuvent normalement être utilisés à la suite d'un `make` (voir le sous-dossier bin).
Par conséquent le point 4 est à éviter, sauf si vous pensez que le logiciel peut être utile ou est déjà utilisé par le reste de la communauté. Dans ce dernier cas, merci de bien préciser dans votre ticket l'adresse officielle du logiciel et de vérifier au préalable les conditions d'utilisation du logiciel _(y a t'il une restriction d'utilisation, le besoin d'utiliser une licence non libre, spécifique... ? Ce logiciel peut-il s'installer sous Linux ?...)_.

Si vous avez quand même besoin de privilèges élevés, il faudra faire un conteneur [*Singularity*](singularity.md).

Dans le cas d'un conteneur Singularity que vous avez créé et que vous pensez être utile pour la plupart des utilisateurs, merci de nous en informer en faisant un [ticket](http://kimura.univ-montp2.fr/aide/index.php?a=add).

Dans le cas où le logiciel vous semble trop complexe à installer, nous l'installerons dans votre home ou créerons une solution appropriée.

>**Note** Pour les packages/modules Perl, Python ou R, nous n'installons plus en standard ces derniers sur toutes les machines. Cette installation est à votre charge, sauf en cas de dépendances systèmes (librairies systèmes ou packages système autre (rpm) etc...). Voir :
  - [installation d'un module perl](perl.md#installation-de-modules-perl)
  - [installation d'un module python](python.md#installer-un-module-python)
  - [installation d'un package R](r.md#(#installer-un-package-r))

## PATH

Cette variable d'environnement Linux permet de lister tous les chemins censés contenir des binaires. Ainsi, vous pouvez utiliser des binaires, sans utiliser de chemin vers ces derniers, mais en les appelant directement.

Vous pouvez afficher son contenu simplement :

```bash
echo $PATH
```

Vous verrez ainsi les chemins les plus courants qui contiennent des binaires (`/usr/bin`, `/bin`, `/usr/local/bin` (...)). Ces chemins sont séparés par le caractère `:`.

Vous pouvez la modifiant avec la commande suivante :

```bash
export PATH=$PATH:/autre_chemin_ves_binaires
```

Avec `/autre_chemin_ves_binaires`, le chemin à rajouter au PATH pour rechercher des binaires.

Il peut être pratique de rajouter ce type de ligne à la fin de votre fichier `~/.bashrc` pour que ça se remette à chaque reconnexion de votre part.

Sinon, il faudra utiliser un chemin relatif ou absolu pour pouvoir lancer votre commande ([notions de chemins](http://casteyde.christian.free.fr/system/linux/guide/online/x3198.html)).

## Chercher avec l'auto-completion

Sous Linux, un aspect très pratique est l'_auto-complétion_. Vous pouvez chercher un programme si vous connaissez le début de son nom (puis en appuyant sur la touche `TAB`).

Si ça ne répond pas, soit le logiciel n'est pas présent, soit il est installé, mais les binaires ne sont pas contenus dans le PATH. C'est le cas de certains logiciels installés dans /usr/local/, dans /opt ou dans votre dossier personnel /home/_nom\_d\_utilisateur_.

> **Warning** Le noeud maître est très allégé en programme! En effet, il est volontairement bridé pour qu'il reste disponible / pour que vous puissiez vous y connecter.

Si vous ne trouvez pas le programme que vous cherchez directement sur le noeud maître, vous devez essayer de le trouver sur les noeuds de calcul! Il convient donc de faire un `qrsh` au préalable, afin d'être connecté sur un noeud de calcul; vous pouvez ensuite réessayer de faire de l'_auto-complétion_.

> **Caution** A noter qu'avec qrsh, vos variables d'environnement sont modifiées, le `PATH` n'est donc pas tout à fait le même. Par ailleurs, pensez à faire un logout/exit à la fin de votre recherche. En effet, tous les jobs (sauf vos compilations / installations) doivent être lancés par qsub depuis le noeud maître.

## Chercher avec which, whereis, find, locate

Vous pouvez utiliser toutes les commandes listées ci-dessus pour chercher un fichier exécutable :

* [which](http://www.computerhope.com/unix/uwhich.htm)
* [whereis](http://www.computerhope.com/unix/uwhereis.htm)
* [find](http://www.linuxcertif.com/doc/keyword/find/)

La commande _which_ retroune le premier chemin valide trouvé du PATH, alors que _whereis_ continuera de chercher dans le _PATH_.

> **Warning** Comme pour l'_auto-complétion_, ci-dessus, sachez que le noeud maître est allégé en programme. Pensez à faire un `qrsh` pour vérifier la présence du logiciel à chercher sur les noeuds de calcul.

## /share/apps/bin

Ce dossier historique contient un grand nombre de logiciels. Etant composé d'un grand nombre de sous-dossiers, nous n'ajoutons pas tous ces sous-dossiers à votre PATH. Un simple :

```bash
ls /share/apps/bin
```

permet déjà de se faire une idée.

> **Info** Voir aussi l'utilisation de [module](#utiliser-module)

<a id="install-cluster-software"></a>

# Installer un logiciel

En tant qu'utilisateur vous avez des droits limités. Vous pouvez installer ce que vous voulez dans votre dossier personnel. Vous pouvez également le tester dans le répertoire /tmp. Certaines commandes nécessitent généralement des privilèges plus élevés, comme :

```bash
make install

# specifique à debian et ses dérivés
dpkg -i...
gdebi ...
apt install ...

# spécifique à RedHat et ses dérivés
rpm -i ...
yum install ...
```

Parfois, des binaires sont directement disponibles en téléchargement pour votre logiciel. Dans ce cas, vous pouvez vous contenter de copier ce fichier _(version amd64 bits)_ dans votre Home et celà peut suffire pour l'exécuter.

## make, make install

Un simple `make` suffit souvent pour faire fonctionner votre programme (parfois le `make install` ne fait que copier des binaires et des librairies dans des chemins systèmes plus standards (`/usr/bin`, etc...)). Par ailleurs, si vous avez pu faire un `./configure` en passant l'option `--prefix=~/chemin_vers_mon_nouveau_logiciel`, il est possible que `make install` fonctionne. Tout dépendra des règles qui sont définies dans le fichier `makefile`.

## Package deb ou rpm

Pour ce type d'installation, seul le _root_ est habilité à installer ces programmes. Cependant, vous pouvez peut-être trouver des sources plus à jour du logiciel que vous cherchez (non figées dans un package) et l'installer depuis les sources (souvent une archive compressée de type .tar.gz, .tgz, .tar.bz2 ou en clonant un dépôt...).

## Namespaces, conteneurs, singularity

Singularity, par l'utilisation de techniques utilisées par les technologies de conteneurs, telles que les `namespaces` ou `cgroups` permet de contourner certaines difficultés d'installation de packages.

Merci de vous reporter à la [documentation dédiée](singularity.md) pour plus d'informations.

## Installation de package R

> **Info** En premier lieu, merci de lire comment [utiliser module](#utiliser-module)

R est disponible au travers de module.

Merci de vous référer à cet article: [Installer un package R](r.md#installer-un-package-r)

## Installation de package Perl

> **Info** En premier lieu, merci de lire comment [utiliser module](#utiliser-module)

Perl est disponible au travers de module. Il s'agit de la version 5.14

Merci de lire [cet article](http://linuxgazette.net/139/okopnik.html) pour personnaliser votre environnement et utiliser des packages R depuis votre Home.

## Installation de package Python

> **Info** En premier lieu, merci de lire comment [utiliser module](#utiliser-module)

Python est disponible au travers de module.

[Voir ici](python.md#installer-un-module-python) pour installer un package python.

# Utiliser module

module est un système qui permet de changer rapidement d'un environnement à un autre pour des programmes ou des librairies. Pour celà, on utilise des "modulefiles". Pour l'instant, les modulefiles disponibles sont accessibles par la commande :

```bash
module avail
```

Comment l'utiliser ?

```bash
$ module add R/3.5.3
$ R --version
R version 3.5.3 (2019-03-11) -- "Great Truth"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)
...
$ which R
/share/apps/sing-images/3.1/R
$ module list
Currently Loaded Modules:
  1) singularity/3.1   2) R/3.5.3
$ module rm R/3.5.3
$ module list
No modules loaded

$ R --version
R version 3.4.4 (2018-03-15) -- "Someone to Lean On"
Copyright (C) 2018 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)
...
$ which R
/usr/bin/R

$ module load R/3.2.0
$ R --version
R version 3.2.0 (2015-04-16) -- "Full of Ingredients"
Copyright (C) 2015 The R Foundation for Statistical Computing
Platform: x86_64-unknown-linux-gnu (64-bit)
...
$ which R
/share/apps/bin/R/R-3.2.0/bin/R
$ module list

Currently Loaded Modules:
  1) R/3.2.0

$ module rm R/3.2.0

$ module load java1.8
$ which java
/etc/alternatives/jre_1.8.0_openjdk/bin/java
$ module show java1.8
-------------------------------------------------------------------
/etc/modulefiles/java1.8:

setenv JAVA_HOME /etc/alternatives/jre_1.8.0_openjdk
prepend-path PATH /etc/alternatives/jre_1.8.0_openjdk/bin/
prepend-path LD_LIBRARY_PATH /etc/alternatives/jre_1.8.0_openjdk/lib/
append-path PE_PRODUCT_LIST JAVA
-------------------------------------------------------------------

$ echo $PATH
/etc/alternatives/jre_1.8.0_openjdk/bin/:/usr/local/bin:....
$ echo $LD_LIBRARY_PATH
/etc/alternatives/jre_1.8.0_openjdk/lib/:/opt/gridengine/lib/linux-x64:/opt/openmpi/lib:/lib64:/usr/lib64:/usr/local/lib64:/usr/lib/:/usr/local/lib:/opt/python/lib
$ module load gcc4.9
$ module list
Currently Loaded Modulefiles:
1) rocks-openmpi 2) java1.8 3) gcc4.9
```

>**Caution** Dans vos scripts de soumission ```qsub```, pensez à précéder la ligne d'exécution de votre script par le chargement de la version que vous souhaitez utiliser avec module, ou bien soumettez votre job/script de soumission avec l'option "-V" après avoir chargé votre environnement avec module dans votre shell.

Exemple :

```bash
module load R-3.2.0

# l'option "-V" peut aussi être rajoutée à la liste des options SGE dans le script de soumission ($# -V).
# Cf le fichier general_qsub_file.sh dans votre home.

qsub -V mon_script_qsub.sh

# avec mon_script_qsub.sh qui contient, par ex. :
#$ -S /bin/bash
#$ -cwd
#$ -N myRscript
#$ -o my_output_file
R CMD BATCH my_script.R
```

ou :

```bash
qsub mon_script_qsub.sh

# avec mon_script_qsub.sh qui contient, par ex. :
#$ -S /bin/bash
#$ -cwd
#$ -N myRscript
#$ -o my_output_file
module load R-3.2.0
R CMD BATCH my_script.R
```

# Comment connaître les environnements parallèles disponibles sur le cluster

Faire `qconf -spl` pour avoir la liste de ces environnements puis `qconf -sp nom_du_pe` pour en connaitre la définition plus précisément la propriété *allocation_rule* qui peut être :
    1. "pe_slots", i.e., tous les slots sont alloués sur le même noeud,
    2. "round robin", i.e. les slots sont distribués sur les noeuds un à la fois à tour de rôle,
    3. "fill up", i.e. les slots sont alloués sur un premier noeud jusqu'à son remplissage avant de passer aux suivants ...

Pour savoir quels environnements parallèles sont disponibles sur une queue de calcul, il faut afficher les propriétés de la queue et filtrer sur les environnements parallèles :

```bash
qconf -sq <nom_de_la_queue> |grep pe_
#exemple
qconf -sq mem.q |grep -A1 pe_
pe_list               make mpi mpich multithread60 multithread8 orte robin \
                      thread
```

# Limitations sur le cluster ISEM et le cluster MBB

Par défaut plusieurs types de limitations peuvent s'appliquer :

 1. quota disque,
 2. accès ou non à certaines queues,
 3. limitation en temps de calcul sur les queues,
 4. limitation en nombre de jobs par queue ou par utilisateur.

## Comment connaître ces limites

### Quotas disques

Votre quota par défaut sur le cluster BlueBanquise est de 250Go.

Pour connaître votre quota défini dans l'annuaire :

```bash
get_myquota.sh
```

Ce script va vous demander votre mot de passe pour interroger la base de données. Il vous propose également de calculer votre utilisation actuelle de disque. Par défaut, tous les anciens comptes ont une limite à 70Go, mais ce n'est plus le cas sur le nouveau cluster (voir ci-dessus). Il est alors fort probable que la valeur ne soit pas à jour, et vous pourrez quand même utiliser jusqu'à 250Go.

### Acces aux queues

Pour savoir à quelles queues/machines vous avez accès :

```bash
qselect -U $USER
```

Attention la réponse de `qselect` se fait en fonction de la charge en temps réel. Ainsi, si toutes les machines sont pleines, `qselect` ne répondra rien...

### Limite en temps

Plusieurs paramètres dans SGE permettent de limiter le temps de calcul des jobs sur les clusters. Nous en utilisons deux :

* la limite par `h_rt` (hardware runtime),
* la limite définie par la queue elle-même.

#### Connaitre le h_rt

```bash
remy@cluster-mbb:~$ tail -1 $SGE_ROOT/$SGE_CELL/common/sge_request
-l h_rt=12:00:00
```

Par défaut, tout job sera donc tué au bout de 12h. Cependant, vous pouvez écraser cette valeur en mettant celle de votre choix en changeant cette option quand vous soumettez un job. Il est possible de marquer par exemple :

```bash
#$ -l h_rt=999:00:00
```

Vous pouvez aussi spécifier cette valeur en secondes.

Vous pouvez aussi écraser cette valeur pour toujours en créant un fichier nommé `.sge_request` à la racine de votre _home directory_.

>**Info** Auparavant, il fallait aussi spécifier le `s_rt` (software runtime), mais ce n'est plus nécessaire.

#### Connaître les caracteristiques d'une queue

Lorsque vous savez sur quelle(s) queue(s) vous pouvez tourner (voir [ci-dessus](#acces-aux-queues), avec `qselect -U $USER`), il vous suffit de faire par exemple :

```bash
qconf -sq <nom_de_queue>
```

>**Info** Vous pouvez connaître l'ensemble des queues présentes avec `qconf -sql`

Ainsi, sur la queue ISE-M _small.q_, une limite de 10h a été fixée :

```bash
qconf -sq small.q |grep h_rt
h_rt                  10:00:00
```

## Limite par nombre de jobs

Afin de savoir si vous êtes limités en nombre de jobs simultanés, il vous faut d'abord savoir s'il existe des "ressources quotas" dans SGE :

```bash
qconf -srqsl
```

Lorsque vous avez la liste des "ressources quotas", vous pouvez afficher la caractéristique de chacune avec :

```bash
qconf -srqs <ma_ressource_quota>
```

Exemple :

```bash
qconf -srqs limit_slots_to_cores_rqs
{
   name         limit_slots_to_cores_rqs
   description  Prevents core oversubscription across queues.
   enabled      TRUE
   limit        hosts {*} to slots=$num_proc
}
qconf -srqs max_slots_on_sshqueues
{
   name         max_slots_on_sshqueues
   description  "limit the number of slots for queues cemeb(20).q for ssh \
   submissions"
   enabled      TRUE
   limit        users {*} queues {cemeb.q,cemeb20.q} to slots=80
}
```

Dans l'exemple ci-dessus, on remarque qu'on ne peut pas lancer plus de jobs qu'il n'y a de coeurs par noeud et les queues cemeb.q et cemeb20.q du cluster MBB sont limitées à 80 jobs/utilisateur.

# Parallelisation avec R sur le cluster

Voir [cet article](r.md#parallelisation-avec-r-sur-le-cluster)

# File staging : Allègement cluster, travailler sur les disques des noeuds de calcul

Les home directory du noeud maître sont exportés via NFS sur les noeuds de calcul pour vous faciliter le travail.
Cependant, les applications nécessitant beaucoup d'accès disque ( blast, NGS, ... ) dégradent les performances du système.

Pour y remédier, vous devez adopter la démarche suivante :
  - Chaque noeud dispose d'un espace disque qui lui est propre. La taille de cet espace varie en fonction des noeuds de calcul (56G ou 250G),
  - Vérifier l'existence du dossier /export/scrach/$USER (avec $USER étant vôtre login). Si ce n'est pas le cas, le créer par la commande :

```bash
mkdir /export/scrach/$USER
```

  - Copier les fichiers à utiliser dans un emplacement temporaire de ce dossier (exemple: /export/scrach/$USER/tmp),
  - Réaliser les opérations de lecture et d'écriture dans ce dossier,
  - Une fois les traitements effectués, vous pouvez copier les résultats attendus dans votre dossier personnel,
  - Surtout ne pas oublier de supprimer son dossier temporaire une fois les jobs terminés.

Voici un exemple de script sge utilisant ce dossier :

```bash
#!/bin/bash
#
#$ -S /bin/bash
#$ -cwd
#$ -e job_name.err
#$ -o job_name.out

# On créé notre dossier temporaire
mkdir -p /export/scrach/khalid/tmp

# On copie nos fichiers de travail dans notre dossier temporaire
cp /home/khalid/monfichier /export/scrach/khalid/tmp

# On exécute notre programme en travaillant dans le dossier temporaire
test.pl /export/scrach/khalid/tmp/monfichier /export/scrach/khalid/tmp/job_output

# On copie nos résultats, et on n'oublie pas de supprimer son dossier temporaire
cp -rp /export/scrach/khalid/tmp /home/$USER
rm -rf /export/scrach/khalid/tmp
```

# Synchronisation des jobs et nombres aleatoires

Les simulations c’est bien, car ça se parallélise facilement… sauf quand qsub les lance simultanément, et que du coup il calcule plusieurs fois la même chose car chaque simulation utilise la même graine aléatoire.

La solution propre, c’est de générer un index de graines que les jobs consultent en temps voulu.

La solution « Quick & Dirty (c) », mais au combien pratique, c’est de rajouter cette ligne dans votre script bash soumis :

```bash
n=$RANDOM; let « n %= 60 »; sleep $n
```

Ceci va attendre un temps aléatoire entre 0 et 60s avant de lancer le programme, ce qui devrait résoudre bien des problèmes !

# Abyss en parallele

Lancer la version MPI d’Abyss :

exemple de script : submit_abyss.sh

```bash
#!/bin/bash
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -pe robin 8
#$ -N tst_abyss

fic=$1
kmer=$2
outname=$3

export PATH=$PATH:/opt/openmpi/bin/

abyss-pe k=$kmer n=10 in="$fic" name=$outname
```

Lancer sur données _simple end_ :

```bash
qsub submit_abyss.sh reads.fastq 25 test
```

Lancer sur données « paired-end » :

```bash
qsub submit_abyss.sh 'reads_1.fastq reads_2.fastq' 25 test
```

# Mapper des reads en parallele

Une solution intéressante pour distribuer la charge de travail de quelques outils de mapping (bwa, bowtie, soap …) sur plusieurs noeuds du cluster est « pMap: Parallel Sequence Mapping Tool » :http://bmi.osu.edu/hpc/software/pmap/pmap.html .

Une installation est faite sur /share/apps/pmap.

Exemple d’utilisation avec bwa :

Indexation avec ce script _indexer.sh_ :

```bash
#!/bin/bash
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -N pmap_idx_bwa

export PATH=$PATH:/usr/local/bin:/share/apps/pmap/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/SUNWhpc/HPC8.2.1/gnu/lib/lib64/

genomefile=$1
indexdir=$2
indexprefix=$3
pmap_index $genomefile $indexdir $indexprefix bwa -a bwtsw
```

```bash
qsub  indexer.sh speciesA.diploid.fa indexdir/ speciesA
```

Mapping de données paired-end avec ce script « mapper.sh »

```bash
#!/bin/bash
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -pe mpirobin 4
#$ -N pmap_bwa

workdir=/tmp
readsfile=$1
readsfile2=$2
indexdir=$3
indexprefix=$4
outdir=$5

export PATH=$PATH:/opt/openmpi/bin/:/usr/local/bin:/share/apps/pmap/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/SUNWhpc/HPC8.2.1/gnu/lib/lib64/

mpiexec -n 4 pmap_dist $workdir $outdir $readsfile -r $readsfile2 -i $indexdir $indexprefix

#spécifier le chemin car un autre pmap existe dans le path
mpiexec -n 4 /share/apps/pmap/pmap -pe $workdir $outdir bwa
```

```bash
qsub mapper.sh ../reads1.fastq ../reads2.fastq indexdir/ speciesA resu/
```

# wodirqstat

_wodirqstat_: ce programme en python permet de voir quels jobs tournent à quel endroit (noeud, queue, sge working directory).

Son utilisation est assez simple : sans option, ça liste tout. Sinon, il y a la possibilité de rajouter « -u username » afin d’afficher le résultat pour un utilisateur particulier.
