<!-- toc -->
# Formations
- [Phylogénétique et génétique des populations avec R](http://mbb.univ-montp2.fr/MBB/uploads/diapo_Montpellier_2014.pdf)
- [Introduction à Bash et au Cluster MBB](http://gitlab.mbb.cnrs.fr/f/site/form1/readme/)
- [Manipulation de données massives avec un cluster sous Linux](http://gitlab.mbb.cnrs.fr/f/site/form2/readme/)
- [TP RocksCluster](https://gitlab.mbb.cnrs.fr/mbb/tp_rocks/)
- [TP Singlarity avancé](https://mbb.univ-montp2.fr/ust4hpc/tpSingu.html)
- [Liste](https://gitlab.mbb.cnrs.fr/f/site/readme/) des formations ayant été dispensées par MBB

# Migration de RocksCluster à Ubuntu
- [Migration du cluster (été 2019)](https://kimura.univ-montp2.fr/remydernat/migrationclusters.html)
