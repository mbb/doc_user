<!-- toc -->

# Module

>**Warning** Depuis l'été 2015, le cluster MBB et celui de l'ISE-M sont passés sous module pour gérer perl (voir [lien](cluster.md#utiliser-module)). Il faut donc penser à précéder votre ligne de commande par ```module load perl...``` dans votre script de soumission SGE (points de suspension correspondants à la version désirée).

# Environnement Perl

Perl permet de charger une variable d'environnement $PERLLIB / $PERL5LIB qui va chercher les librairies supplémentaires à cet emplacement. Il faut utiliser la commande suivante pour voir si elle est définie:

```bash
module load perl5.14
perl -V |egrep PERL5?LIB
```

Vous pouvez vous-même modifier cette variable d'environnement en la surchargeant pour que d'autres chemins soient pris en charge.

Exemple:

```bash
export PERL5LIB=/home/toto/lib/perl5/5.14.2:$PERL5LIB
```

# Installation de modules Perl

Pour installer des modules perl, à nouveau, il faudra utiliser votre home. Quelques liens pour vous y aider :

- http://alumni.soe.ucsc.edu/~you/notes/perl-module-install.html
- http://blogs.perl.org/users/marc_sebastian_jakobs/2009/11/how-to-install-perl-modules-from-cpan-in-the-unix-user-space.html
- http://linuxgazette.net/139/okopnik.html

# Installation de différentes versions de Perl avec plenv

[`plenv`](https://github.com/tokuhirom/plenv) vous permet d'installer vos propres versions de `Perl`, dans votre $HOME et ainsi choisir l'installation et les binaires qui vous intéressent selon votre besoin, de la même manière que [`pyenv`](https://github.com/pyenv/pyenv) pour python.

```bash
git clone https://github.com/tokuhirom/plenv.git ~/.plenv
git clone https://github.com/tokuhirom/Perl-Build.git ~/.plenv/plugins/perl-build/
/home/loggin/.plenv/bin/plenv install 5.18.2 -Dusethreads
/home/loggin/.plenv/bin/plenv global 5.18.2
```

L'installation de perl prend un peu de temps.

Pour utiliser perl cela sera maintenant :

```bash
/home/loggin/.plenv/bin/plenv exec perl ./hybridScaffold.pl
```

> Au besoin, vous pouvez aussi regarder du côté de [`perlbrew`](https://perlbrew.pl/), qui fait à peu près la même chose que `plenv`.

## Pour installer un module si besoin

```bash
/home/loggin/.plenv/bin/plenv install-cpanm MyPackage
```

[La doc de plenv](https://github.com/tokuhirom/plenv)

## Installer un package avec dependances

Des dépendances importantes nécessiteront de passer par un [conteneur singularity](singularity.md). En effet, nous ne pouvons pas surcharger les disques de tous les noeuds en installations nombreuses et parfois exotiques (avec parfois des problèmes de stabilité, de non compatibilité entre packages ou de dépendances croisées)... 
