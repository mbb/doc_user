<!-- toc -->

# Compter le nombre de séquences dans un fichier fasta

```
cat fichier.fasta | grep '>' | wc -l
```
ou plus simplement :

```
grep -c '^>' fichier.fasta
```

# Fusionner deux lignes d'un fichier texte

Exemple avec sed :

```
sed '$!N;s/\n/\t/' fichier.txt
```
Exemple avec awk :
```
awk  '{
if (NR % 2)
printf("%s", $0)
else
printf("\t%s\n", $0)
}' fichier.txt
```

# Supprimer des lignes vides d'un fichier

```
sed '/^$/d' monfichier.txt
```

# Convertir des fichiers (textes) windows au format linux

Dans un terminal, utiliser la commande

```
sed -i ‘s/^M$//’ monfichier
```

où ^M s’obtient en faisant CTRL+V CTRL+M

# Supprimer une séquence d’un fichier Fasta (à l’aide d’awk)

Supprimer une séquence en donnant son nom (sans le signe >)

```
seqname=bad_seq
awk ‘{
    if ( $0 ~ /^>’$seqname’$/ ){
        seq=1 ;
        while( seq == 1 ){
            getline;
            pos = index($0,’>’);
            if ( pos == 1 ){
                seq=0;
                print $0;
            }
        }
    }
    else print $0;
}
‘ test.fasta
```

# Remplacement dans les lignes des fichiers



Changer le caractère des gaps de '-' à '*'

``` bash
sed ‘s/-/*/g’ test.fasta
```
Faire le changement dans plusieurs fichiers d’un dossier :
``` bash
find /dossier/contenant/fichiers -type f -exec sed -i ‘s/-/*/g’ {} \;
```
Changer les signes de début de séquence ‘>’ en ‘#’
``` bash
sed ‘s/^>/#/’ test.fasta
```

# Supprimer certaines lignes d’un fichier

Supprimer la ligne 3 d’un fichier
``` bash
sed ‘3d’ test.fasta
```
Supprime entre le ligne 3 et 10 d’un fichier

``` bash
sed ‘3,10d’ test.fasta
```

Supprime les noms des séquences d’un fichier fasta

``` bash
sed ‘/^>/d’ test.fasta
```

# Imprimer certaines lignes d’un fichier

Imprimer la ligne numéro 5
```
sed -n ‘5p’ test.fasta
```

Imprimer de la ligne numéro 5 à la ligne 10

```
sed -n ‘5,10p’ test.fasta
```

Imprimer juste les noms des séquences au format fasta

```
sed -n ‘/^>/p’ test.fasta
```

Imprimer juste les séquences sans les noms

```
sed -n ‘/^>/!p’ test.fasta
```

Imprimer les lignes avec 3 gaps ou plus

```
sed -n ‘/-\{3\}/p’ test.fasta
```

# Afficher la taille de chaque séquence d’un fichier Fasta
``` bash
while  read ligne do
    if `echo ${ligne} | grep ‘^>’ 1>/dev/null 2>&1` then
        echo $ligne
    else
        echo $ligne | wc -m
    fi
done < test.fasta
```
