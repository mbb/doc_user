<!-- toc -->

# TERM

Si vous avez, comme moi, l'habitude d'utiliser `vi` ou `vim`, vous allez voir que ça pose problème sur les noeuds de calcul si vous avez fait un `qrsh` avant.

En fait, ça vient de la définition de la variable d'environnement TERM, qui ne semble pas initialisé correctement dans tous les cas.

Vous pouvez corriger ça assez simplement avec la commande suivante :

```bash
export TERM=xterm
```

Pour ne plus avoir le problème, vous pouvez même le rajouter à votre fichier `.bashrc`:

```bash
echo 'export TERM=xterm' >> ~/.bashrc
exec $SHELL
```

Et voilà !

# Transfert de fichier avec croc

[`croc`](https://github.com/schollz/croc) est un petit code en `go` permettant de tranférer des données et qui traverse les pare-feu. Bien que `scp`, `sftp` ou encore `rsync` suffisent dans la plupart des cas, il se peut qu'exceptionnellement ça ne soit pas suffisant.

Voici comment l'installer dans votre _$HOME_ sur notre cluster :

```bash
cd
module load golang/1.13.1
GO111MODULE=on go get -v github.com/schollz/croc/v8
```

Ensuite, sur une machine depuis laquelle vous voulez transférer des données :

```bash
# après avoir installé croc sur votre machine
croc send <fichier-ou-dossier>
# vous copiez le mot de passe donné
```

Puis du côté de votre _$HOME_ sur le cluster :

```bash
# en utilisant le mot de pass donné
# <baaar-blob-fooo> à remplacer par cee dernier
cd
./go/bin/croc <baaar-blob-fooo>
```

# Bugs singularity

Si vous avez le message suivant :

```text
FATAL: kernel too old
```

Essayez de changer l'image de départ de votre conteneur pour un noyau linux plus ancien.

Par exemple, si vous faites, dans votre recette, un `From: ubuntu:18.04`, essayez de le remplacer par `From: ubuntu:16.04`.

# Changer de tmpdir avec singularity

```bash
mkdir ~/tmp
module load singularity
export SINGULARITY_TMPDIR=/home/$USER/tmp
singularity build forestatrisk-tropics.simg docker://ghislainv/docker-forestatrisk-tropics
```
