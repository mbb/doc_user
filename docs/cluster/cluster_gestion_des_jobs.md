# Gérer ses jobs sous SLURM


Supprimer un job :

```bash
scancel <JOB_ID>
```

L'accounting slurm correspond à nos utilisateurs sur le cluster + des groupes Linux.

Par conséquent, pour supprimer tous ses jobs, il suffit de lancer :

```bash
scancel -u $USER
```

Voir son historique de jobs :

```bash
sacct -u $USER
```

Avoir des informations sur un job :

```bash
squeue -j <JOB_ID>
sacct -j <JOB_ID>
sacct -l -j <JOB_ID>
scontrol show job <JOB_ID>
```

> Merci de penser à remplacer `<JOB_ID>` par votre numéro de job en supprimant les chevrons (symboles inférieurs `<` et supérieurs `>`).

`scontrol` purge les jobs assez rapidement, contrairement à `sacct`; pour avoir uniquement quelques colonnes avec sacct sur un job :

```bash
sacct  --format=jobid,MaxRSS,MaxVMSize,start,end,CPUTImeRaw,NodeList,ReqCPUS,ReqMem,Elapsed,Timelimit,State,ExitCode -j <JOB_ID>
```


> Pour `sacct`, [voir aussi](https://wiki.incd.pt/books/manage-jobs/page/sacct) ou `man sacct`.

## Avoir des informations par partition

```bash
# ici, sur la queue small : affichage de vos jobs par état
squeue -u $USER -p small -t RUNNING
squeue -u $USER -p small -t PENDING
squeue -u $USER -p small -t COMPLETED
```

> **Info** Vous pouvez désormais passer à la section pour [utiliser module](cluster_module.md)
