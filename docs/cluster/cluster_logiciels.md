<!-- toc -->

# Logiciels sur le cluster

## Connaître la liste des logiciels installés

De nombreux logiciels sont déjà présents dans le dossier `/share/apps/bin`. Ce dossier est visible sur tous les noeuds de calcul. Un simple `ls /share/apps/bin` permet donc de se faire une 1ère idée de la liste disponible.

La commande [module](cluster_module.md#utiliser-module) permet également de charger certains logiciels. Pour l'instant, peu de modules ont été porté vers le nouveau cluster. Les anciens modules présents dans `deprecated/` n'ont pas été testé.

Il est parfois possible qu'une version packagée de votre logiciel existe avec `guix`. Si c'est le cas, nous vous recommandons d'utiliser cette version. Merci de vous rapportez à [la section correspondante](../guix.md). D'autres logiciels existent parfois sous forme de conteneur docker ou [singularity](../singularity.md) ou [conda](../python.md#installer-conda). Des techniques vous permettent alors d'installer des logiciels sans passer par l'administrateur du cluster.

## Que faire lorsque le logiciel n'est pas présent

Quand le logiciel n'est pas présent, nous vous demandons alors de faire un conteneur ([voir documentation Singularity](../singularity.md)) (au pire, nous pouvons aider à le faire).

En dernier recours, nous installerons le logiciel directement sur les noeuds (dans `/usr/local/bin` ou `/usr/local/nom_du_logiciel`).

Par conséquent, si l'application que vous souhaitez utiliser n'est pas disponible (ni en standard, ni dans `guix` ou en conteneur), les choix qui s'offrent à vous sont :

  1. Créer un conteneur `singularity` du programme avec ses dépendances (si un `docker` existe pour ce programme, vous pouvez construire un conteneur `singularity` depuis ce dernier),
  2. L'installer dans votre home quand c'est possible (à préférer). Votre répertoire personnel (home) est visible sur tous les noeuds,
  3. Demander de l'aide [en faisant un ticket](http://kimura.univ-montp2.fr/aide/index.php?a=add)
      1. pour la création du conteneur `singularity`,
      2. pour que l'administrateur le mette à disposition dans `/share/apps/bin` ou dans votre home.

Dans la grande majorité des cas, vous n'aurez pas besoin de privilèges élevés pour installer et utiliser un logiciel sur le cluster. Même ceux qui nécessitent un `make install` peuvent normalement être utilisés à la suite d'un `make` (voir dans le sous-dossier `bin`).

Par conséquent le point 3.2 est à éviter, sauf si vous pensez que le logiciel peut être utile à d'autres. Dans ce dernier cas, merci de bien préciser dans votre ticket l'adresse officielle du logiciel et de vérifier au préalable les conditions d'utilisation du logiciel _(y a t'il une restriction d'utilisation ? Une licence non libre ? Ce logiciel peut-il s'installer sous Linux ? etc...)_.

Si vous avez quand même besoin de privilèges élevés, il faudra, de préférence, faire un conteneur [`Singularity`](../singularity.md).

Dans le cas d'un conteneur Singularity que vous avez créé et que vous pensez être utile pour la plupart des utilisateurs, merci de nous en informer en faisant un [ticket](http://kimura.univ-montp2.fr/aide/index.php?a=add). Nous le partagerons et créerons un module associé.

> **Note** Pour les packages/modules Perl, Python ou R, nous n'installons plus en standard ces derniers sur toutes les machines. Cette installation est à votre charge, sauf en cas de dépendances systèmes (librairies systèmes ou packages système autre (rpm) etc...). Voir :

* [installation d'un module perl](../perl.md#installation-de-modules-perl),
* [installation d'un module python](../python.md#installer-un-module-python),
* [installation d'un package R](../r.md#(#installer-un-package-r))

## PATH

Cette variable d'environnement Linux permet de lister tous les chemins censés contenir des binaires. Ainsi, vous pouvez utiliser des binaires, sans utiliser de chemin vers ces derniers, mais en les appelant directement.

Vous pouvez afficher son contenu simplement :

```bash
echo $PATH
```

Vous verrez ainsi les chemins les plus courants qui contiennent des binaires (`/usr/bin`, `/bin`, `/usr/local/bin`, etc). Ces chemins sont séparés par le caractère `:`.

Vous pouvez la modifiant avec la commande suivante :

```bash
export PATH=$PATH:~/autre/chemin/vers/mes/binaires
```

Avec `~/autre/chemin/vers/mes/binaires`, le chemin à rajouter au PATH pour rechercher des binaires.

Il peut être pratique de rajouter ce type de ligne à la fin de votre fichier `~/.bashrc` pour que ça se remette à chacune de vos reconnexion.

Sinon, il faudra utiliser un chemin relatif ou absolu pour pouvoir lancer votre commande ([notions de chemins](http://casteyde.christian.free.fr/system/linux/guide/online/x3198.html)).

## Chercher avec l'auto-completion

Sous Linux, un aspect très pratique est l'_auto-complétion_. Vous pouvez chercher un programme si vous connaissez le début de son nom (puis en appuyant sur la touche `TAB`).

Si ça ne répond pas, soit le logiciel n'est pas présent, soit il est installé, mais les binaires ne sont pas contenus dans le PATH. C'est le cas de certains logiciels installés dans `/usr/local/`, dans `/opt` ou dans votre dossier personnel `$HOME`.

> **Warning** Le noeud maître est très allégé en programme ! En effet, il est volontairement bridé pour qu'il reste disponible / pour que vous puissiez vous y connecter.

Si vous ne trouvez pas le programme que vous cherchez directement sur le noeud maître, vous devez essayer de le trouver sur les noeuds de calcul ! Il convient donc de faire un `srun --pty bash` au préalable, afin d'être connecté sur un noeud de calcul; vous pouvez ensuite réessayer de faire de l'_auto-complétion_.

> **Caution** Avec `srun`, pensez à faire un logout/exit à la fin de votre recherche. En effet, tous les jobs (sauf vos compilations / installations) doivent être lancés par `sbatch` depuis le noeud maître.

## Chercher avec which, whereis, find, locate

Vous pouvez utiliser toutes les commandes listées ci-dessus pour chercher un fichier exécutable :

* [which](http://www.computerhope.com/unix/uwhich.htm)
* [whereis](http://www.computerhope.com/unix/uwhereis.htm)
* [find](http://www.linuxcertif.com/doc/keyword/find/)

La commande _which_ retroune le premier chemin valide trouvé du PATH, alors que _whereis_ continuera de chercher dans le _PATH_.

> **Warning** Comme pour l'_auto-complétion_, ci-dessus, sachez que le noeud maître est allégé en programme. Pensez à faire un `srun --pty bash` pour vérifier la présence du logiciel à chercher sur les noeuds de calcul.

> **Info** Voir aussi l'utilisation de [module](cluster_module.md#utiliser-module)

## Package deb ou rpm

Pour ce type d'installation, seul le _root_ est habilité à installer ces programmes. Cependant, vous pouvez peut-être trouver des sources plus à jour du logiciel que vous cherchez (non figées dans un package) et l'installer depuis les sources (souvent une archive compressée de type .tar.gz, .tgz, .tar.bz2 ou en clonant un dépôt...).

_Sachez néanmoins qu'un package reste une archive installable pour une distribution donnée, comprenant des métadonnées (comme l'auteur, la date de réalisation, la liste des dépendances, etc) et qu'il est techniquement possible d'en extraire le contenu._
