<!-- toc -->

# Prise en main rapide du cluster de calcul

## Linux

Merci de vous rapporter à la section [linux](../linux_base.md).

Par ailleurs, il existe de nombreux tutoriels déjà présents sur le Web. Par exemple, [celui-ci](https://learntutorials.net/fr/bash/topic/300/demarrer-avec-bash).

## Démarrer sur le cluster

> Le cluster est actuellement en mode dégradé pour raisons d'économies d'énergie ! Si vous avez besoin de rallumer des noeuds, merci de [faire un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html).

### Utilisation de SLURM

#### Liens utiles

Quelques liens utiles :

* [Guide de conversion SGE](https://srcc.stanford.edu/sge-slurm-conversion),
* [Le guide officiel de démarrage rapide avec SLURM](https://slurm.schedmd.com/quickstart.html),

D'autres ressources en français :

* [Tutoriel Sourcesup](https://sourcesup.renater.fr/wiki/ici-sc-help/tutorials:slurm),
* [Le guide de Southgreen](https://southgreenplatform.github.io/trainings//HPC/),
* [Cette documentation Genotoul](http://bioinfo.genotoul.fr/wp-content/uploads/Bull_slurm_for_users.pdf).

#### Obtenir des informations

SLURM est l'outil de gestion des job utilisé sur le cluster ISEM.

Voir les noeuds de calcul disponibles par files d'attente ou `partition` :

```bash
sinfo
```

On découvre déjà plusieurs informations importantes :

* il y a 3 partitions : `mem`, `long` et `small`,
* la queue par défaut est la queue `long` (car notée **`long*`**),
* on voit l'état des partitions, leur disponibilité, les noeuds qui les composent [\*] et les durées maximales des jobs (exprimée en JJ-HH:MM:SS).

[\*] `valkyrie-[108-109,209-211]` signifie par les exemples les noeuds `valkyrie-108`, `valkyrie-109`, `valkyrie-209`, `valkyrie-210` et `valkyrie-211`.

> Plus d'informations disponibles sur les queues avec `sinfo -l`.

Pour avoir plus de détails par noeud :

```bash
sinfo -N -l
```

Plusieurs états sont possibles :

* _alloc_ : le noeud est entièrement utilisé,
* _mix_ : le noeud est partiellement utilisé,
* _idle_ : le noeud est en attente,
* _drain_ : le noeud termine les jobs en cours mais n'en accepte pas de nouveau,
* _down_ : le noeud ne répond pas.

Pour afficher des informations sur un noeud :

```bash
scontrol show node <nodename>
```

"`<nodename>`" à remplacer par le noeud dont on souhaite connaître les détails.

Voir les partitions et leurs règles :

```bash
squeue
# pour rafraichir toutes les 5s
squeue -i 5
# avec un choix des champs ordonnés
squeue -O "username,name:40,partition,nodelist,cpus-per-task,state,timeused,timelimit"
# verifier ses propres jobs
squeue -u $USER

# avec plus de détails sur les partitions
scontrol show partition
scontrol show partition small
```

Afficher des informations sur les jobs :

```bash
scontrol show job <job_id>
```

## Détails sur les partitions

| Nom de la partition | Nombre de Noeuds | Nombre de coeurs | DefMemPerCPU (mémoire par coeur par défaut) | Temps par défaut | Limite de temps |
| --- | --- | --- | --- | --- | --- |
| small | 5 | 48 | 2048 | 2-00-00-00 | 7-00-00-00 |
| long | 18 | 704 | 1024 | 3-00:00:00 | 120-00:00:00 |
| mem | 2 | 40 | 8192 | 3-00:00:00 | 31-00:00:00 |

_Le temps est exprimé en DD-HH:MM:SS_

> **Hint** L'option `--mem-per-cpu=xG` (avec xG à remplacer) permet d'écraser la variable `DefMemPerCPU`.

> **Info** Vous pouvez désormais passer à la section de [soumission des jobs](cluster_soumission_de_job.md)
