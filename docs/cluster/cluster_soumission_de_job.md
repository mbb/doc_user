# Gestion des jobs

## Soumettre son premier job

### sbatch

La manière la plus standard pour soumettre un job avec SLURM consiste à utiliser `sbatch` (équivalent de `qsub` dans `SGE`).

Voici un petit exemple qui ne fait rien pendant 3 secondes puis se contente d'afficher le nom des noeuds sur lesquels le job tourne :

```bash
#!/bin/bash
#SBATCH --job-name=test_basic
#SBATCH --output=test_basic.out
#SBATCH --error=test_basic.err
#SBATCH --time=24:00:00
#SBATCH --mem-per-cpu=1G
#SBATCH --partition=small
#SBATCH --mail-type=END
#SBATCH --mail-user=<prenom.nom@umontpellier.fr>
###############

sleep 3
hostname
```

> **/!\ Remplacez <prenom.nom@umontpellier.fr> par votre email en pensant à enlever les chevrons (symboles inférieurs et supérieurs `>`, et `<`)**

On écrit ça dans un fichier `test_basic.sbatch` (par exemple avec [`vim`](https://linuxhandbook.com/basic-vim-commands/), [`cat`](https://www.baeldung.com/linux/cat-writing-file), ou simplement [`nano`](https://www.hostinger.fr/tutoriels/nano#Comment_utiliser_lediteur_de_texte_Nano) [\*]) et on le soumet ainsi :

> **Hint** [\*] Si vous copiez un fichier depuis Windows sur le cluster, pensez à utiliser la commande [`dos2unix`](https://linuxstar.info/dos2unix/) pour convertir les retours à la ligne. `mac2unix` permet de faire la même chose depuis un Mac.

```bash
sbatch test_basic.sbatch
```

Le retour doit vous afficher un nombre; c'est votre `JOB_ID`.

Si on regarde en détail le fichier de soumission, on lui a indiqué :

* _un nom de job_,
* _un nom de fichier de sortie standard (`test_basic.out`)_,
* _un nom de fichier de sortie d'erreur (`test_basic.err`)_,
* _une limite de temps de 24h_,
* _une mémoire par coeur de 1Go_,
* _une exécution sur la partition `small`_,
* _l'envoi d'un mail quand le job est terminé_,
* _notre mail (à remplacer par le votre dans ce cas)_.

Suivi de 2 commandes à exécuter sur les 2 noeuds.

>**Caution** Le paramètre `--mem-per-cpu` est très important car il permet au Job Scheduler de déterminer combien de place en mémoire il reste sur un noeud.

L'envoi de mail est à proscrire dans le cadre de tableaux de jobs très nombreux.

> Plus d'informations : `man sbatch`

### Job Interactif avec srun

Vous pouvez soumettre un job de type bash pour vous connecter de manière interactive vers un noeud. Ainsi, vous pouvez aisément tester votre programme avant de le lancer plus proprement avec la méthode précédente.

```bash
# pour ouvrir un shell pendant 1h sur un noeud
srun --time=01:00:00 --pty bash -i
# pour spécifier une queue/partition
srun --time=01:00:00 -p long --pty bash -i
```

Plus d'exemples sur les jobs interactifs [ici](https://www.uibk.ac.at/zid/systeme/hpc-systeme/common/tutorials/slurm-tutorial.html#HDR3).

> Plus d'informations : `man srun`

> **Info** Vous pouvez désormais passer à la section de [gestion des jobs](cluster_gestion_des_jobs.md)
