<!-- toc -->

# Introduction

## Vocabulaire

Un cluster de calcul est un ensemble de machines connectées entre elles qui vont travailler à résoudre des problèmes importants qui seraient trop gourmands pour votre machine personnelle.
Cela peut se faire soit avec une interconnexion réseau ayant une faible latence et une large bande passante (on parle de [cluster HPC](https://www.hpc.iastate.edu/guides/introduction-to-hpc-clusters/what-is-an-hpc-cluster)), soit au travers d'un réseau plus classique (on parle alors d'un [cluster HTC](https://en.wikipedia.org/wiki/High-throughput_computing)).

Il existe d'autres types de clusters en informatique, des cluster de stockage, de virtualisation, etc.

A l'ISEM, côté calcul, nous avons un cluster de calcul de type HTC.

Pour les clusters de calcul, il y a généralement une ou plusieurs machines de connexion (noeud de login), plusieurs noeuds de calcul, un noeud maître et au moins un système de stockage. Le noeud de login, prévu initialement, n'est pas encore connecté au cluster; il faut donc se connecter au noeud maître.

Les noeuds de calcul sont parfois "`headless`", ce qui signifie que tout le système d'exploitation est stocké en RAM; il n'y a rien de stocké en local. Ce n'est pas le cas à l'ISEM.

Le rôle du noeud maître est de répartir au mieux les jobs en fonction des ressources disponibles et des différentes règles établies par l(es)' administrateur(s) du cluster.

## Architecture

Sur notre cluster, il n'y a pas d'espace de stockage temporaire distribué ou centralisé (scratch). Il y a cependant des espaces temporaires locaux à chaque noeud de calcul (montage `/scratch` en plus du `/tmp`).

Votre dossier personnel, ou `Home Directory` (on parle aussi plus simplement de `HOME`), lui est monté sur toutes les machines.

## Historique

Jusqu'en 2021, il y avait le cluster ISEM ET le cluster MBB. Seuls les personnels ISEM et leurs collaborateurs sont désormais autorisés à utiliser le nouveau cluster ISEM, [*`BlueBanquise`*](https://bluebanquise.com/), du nom de la solution utilisée.

> **Warning** Les anciens comptes avec un `UID`<1000 ne pourront pas se connecter. Il faut faire un ticket pour un nouveau compte ou une modification d'UID, ce qui nécessitera ensuite un besoin de réappropriation des données. Vous pouvez connaître votre UID à partir d'une autre machine MBB avec la commande `id`.

## Comparaison avec les autres services disponibles

### MBBWorkflows

[MBB](https://mbb.univ-montp2.fr), à l'aide d'[ISI](https://isi.isem-evolution.fr), propose un service d'aide à la réalisation de workflow avec une plateforme dédiée `MBBWorkflows`. Ce service s'adresse à des personnes qui ont moins de compétences sous Linux ou pour réaliser aux-mêmes leurs workflows. Un stockage accompagné de machines performantes composent cette plateforme.

### Bigmems

Les `bigmems` permettent d'avoir un accès root le temps de l'utilisation de la bigmem, comme dans le cadre d'une machine de cloud. Ce sont des machines très performantes (>256Go de RAM et >48 coeurs) qui peuvent être éventuellement connectées à un système de stockage distant, mais qui, sinon, ont peu d'interconnexions avec les autres machines. Plusieurs To de stockage en local sont également disponibles.
Ainsi, des traitements très gourmands en RAM et peu parallélisés (dans le sens "multinoeuds", et non "multithreads") auront plus leur place sur une bigmem que sur le cluster de calcul.

En dehors des machines privées, il faut penser à récupérer ses données avant la fin de la jouvence de la machine.

### Machines GPU

Deux machines GPU sont disponibles à la réservation. Ces dernières ne sont pas connectées au cluster. Il faut donc faire des demandes sur le système de réservation dédié.
L'application doit être conçue pour tourner sur des cartes GPU.

### Serveur rstudio

Un service rstudio permet de mettre au point des programmes en `R`, avant de les exécuter plus largement sur le cluster.

> **Info** [Plus de détails sur les services historiques](https://mbb.univ-montp2.fr/MBB/subsection/other_services.php) sur le site [MBB](https://mbb.univ-montp2.fr). Pour tous ces autres services, merci de passer par [l'interface de tickets]( https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) pour y accéder.
