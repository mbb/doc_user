# Utiliser module

`Environment modules` et `lmod` sont des systèmes qui permettent de changer rapidement d'un environnement à un autre pour des programmes ou des librairies. Pour celà, on utilise des "`modulefiles`". Pour l'instant, les modulefiles disponibles sont accessibles par la commande :

```bash
module avail
```

> **Warning** Les modules dans `deprecated/` correspondent aux modulefiles des anciens clusters et n'ont pas été testé...

## Comment l'utiliser ?

```bash
$ module add R/3.5.3
$ R --version
R version 3.5.3 (2019-03-11) -- "Great Truth"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)
...
$ which R
/share/apps/sing-images/3.1/R
```

```bash
$ module list
Currently Loaded Modules:
  1) R/3.5.3
$ module rm R/3.5.3
$ module list
No modules loaded
```

A noter qu'un module peut charger et décharger plusieurs autres modules. Pensez à utiliser régulièrement `module list` et consulter votre variable `$PATH` pour voir l'état de votre environnement actuel.

```bash
echo $PATH
# ou, pour afficher toutes les variables d'environnement :
env
```

Dans le cadre de librairies, la variable d'environnement modifiée, est, le plus souvent `$LD_LIBRARY_PATH`.

>**Caution** Dans vos scripts de soumission `sbatch`, pensez à précéder la ligne d'exécution de votre script par le chargement de la version que vous souhaitez utiliser avec module.

Exemple :

```bash
sbatch mon_script.sbatch

# avec mon_script.sbastch qui contient, par ex. :
#!/bin/bash
#SBATCH --job-name=myRscript
#SBATCH --output=myRscript.o
module load R/3.5.3
R CMD BATCH my_script.R
```

## Rafraichir la liste des modules

Régulièrement, des nouveaux modules sont installés ou supprimés; pensez donc à rafraichir la liste des modules avec `module spider`.

Vous pourrez ensuite de nouveau lister les modules disponibles avec `module avail`. N'hésitez pas à filtrer la sortie de cette commande avec `| grep <pattern>` ou directement avec `module keyword <pattern>`; par exemple :

```bash
module keyword bed
```

Sortie :

```
----------------------------------------------------------------------------

The following modules match your search criteria: "bed"
----------------------------------------------------------------------------

  quay.io/biocontainers/bedtools: ...

----------------------------------------------------------------------------

Pour en savoir davantage sur un module exécutez :

   $ module spider Foo

où "Foo" est le nom d'un module.

S'il existe plusieurs version d'un module, vous devez spécifier la version
afin d'avoir l'information détaillée :

   $ module spider Foo/11.1

----------------------------------------------------------------------------
```

## Afficher le contenu d'un modulefile

```bash
module show quay.io/biocontainers/bedtools
```
