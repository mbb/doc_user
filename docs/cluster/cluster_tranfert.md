<!-- toc -->

# Transfert de fichier depuis/vers le cluster ISEM

La méthode classique consiste à utiliser SFTP vers le noeud maître ou directement un NAS. Pour cela, la méthode la plus est d'utiliser le logiciel [FileZilla](https://filezilla-project.org).

Vous pouvez bien entendu aussi utiliser directement un client en ligne de commande, comme [`sftp`](https://linuxize.com/post/how-to-use-linux-sftp-command-to-transfer-files/), [`scp`](https://fr.wikipedia.org/wiki/Secure_copy) ou encore [`rsync`](https://doc.ubuntu-fr.org/rsync).

Si vous transférer des fichiers texte depuis Windows, pensez à utiliser l'utilitaire [`dos2unix`](https://linuxstar.info/dos2unix/) pour convertir vos caractères de fin de ligne. Cela peut également être utile avec des fichiers provenant de MacOS avec `mac2unix` (s'utilise comme `dos2unix`). De la même manière, pour un transfert dans l'autre sens, on pourra utiliser `unix2dos` ou `unix2mac`.

> **Caution** En cas de problème de pare-feu, assez commun pour transférer d'un cluster à l'autre, reportez-vous à [cette section](../tips_and_bugs.md#transfert-de-fichier-avec-croc).

## Avec Teleport

Vous pouvez utiliser `scp` ([qui utilisera `sftp` derrière](https://github.com/gravitational/teleport/issues/7127#issuecomment-1203370077)) avec teleport; la syntaxe est la même qu'avec un `scp` classique, si ce n'est que c'est précédé de `tsh`.

Lorsque le chemin côté destination n'est pas précisé, c'est copié à la racine du HOME de l'utilisateur.

Pour rappel `newmaster.mbb.cnrs.fr` correspond à `newmaster.cluster.local` dans teleport.

```bash
#on se connecte <user> à remplacer
tsh login --proxy=teleport.mbb.cnrs.fr --user=<user>
tsh scp my_file <user>@newmaster.cluster.local:
tsh scp -r my_directory <user>@newmaster.cluster.local:
```

Si besoin, [voir aussi](/doc_user/site/tips_and_bugs/index.html#transfert-de-fichier-avec-croc).
