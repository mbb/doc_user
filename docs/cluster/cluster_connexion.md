<!-- toc -->

# Connexion vers le cluster

## Depuis Windows

Pour vous connecter au cluster depuis Windows, il faut utiliser Putty. Un tutoriel est disponible [ici](http://www.electrictoolbox.com/article/applications/ssh-putty/).

Dans la partie `Host Name` il faudra indiquer `newmaster.mbb.cnrs.fr` et laisser le port à 22 en SSH.

Il faut ensuite accepter la clé SSH du serveur puis il faudra indiquer votre nom de login et votre mot de passe dans le terminal.

En alternative, il existe aussi l'excellent [MobaXterm](https://mobaxterm.mobatek.net/). A défaut, il y a aussi [WinSCP](https://winscp.net/eng/docs/lang:fr) qui permet également de transférer des fichiers.

## Depuis MacOS ou Linux

Vous pouvez vous connecter directement depuis votre terminal :

```bash
ssh <username>@newmaster.mbb.cnrs.fr
```

Avec `<username>` à remplacer par votre nom de login.

## Accès hors site

Le problème est récurrent car nous ne souhaitons pas ouvrir l'accès au cluster au monde entier.

Nous procédons donc en deux étapes :

* soit vous avez un compte UM (connexion avec votre adresse email @umontpellier.fr) ; dans ce cas, il vous suffit d'utiliser le VPN UM et de vous connecter au serveur VPN de l'UM :

  * [Aide à la connexion à l'Université de Montpellier](https://wikidocs.umontpellier.fr/pages/viewpage.action?pageId=1048647)

* soit vous n'avez pas de compte UM. Dans ce cas, il vous faudra faire une demande spécifique au service de la plateforme calcul de l'ISEM à [cette adresse](http://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html), en précisant dans la catégorie "Cluster access", votre groupe de travail et de votre équipe.

> Sous `teleport`, la machine est enregistrée en tant que `newmaster.cluster.local`. Après avoir fait votre login teleport, il faut donc faire `tsh ssh <username>@newmaster.cluster.local`.

### Connexion instable

Le client SSH [mosh](https://mosh.org/) pour MacOS, Android ou GNU/Linux, permet une reprise après incident sur votre connexion; ça évite donc de perdre votre connexion (surtout si vous n'utiliser pas de multiplexer de terminal (type [`tmux`](https://fr.wikipedia.org/wiki/Tmux))).

## Automatisation de la connexion

Afin de faciliter votre connexion sur le cluster ISEM, je vous suggère de générer une clé personnelle sur votre machine (si vous n'en avez pas encore) et de copier la clé publique sur le cluster.

```bash
ssh-keygen -t rsa
ssh-copy-id <username>@newmaster.mbb.cnrs.fr
ssh <username>@newmaster.mbb.cnrs.fr
```

Vous n'aurez plus besoin de taper ensuite votre mot de passe.

Vous pouvez aussi modifier votre fichier `~/.ssh/config` afin d'avoir moins de caratères à rentrer pour vous connecter.

Par exemple :

```bash
cat <<EOF >> ~/.ssh/config
Host nm
    User <username>
    Hostname newmaster.mbb.cnrs.fr
EOF
```

Ici encore, pensez à remplacer `<username>` par votre nom de login. Ensuite, il vous suffira, pour vous connecter, de taper :

```bash
ssh nm
```
