# Alternatives

_Dernière mise à jour 03/05/2024_

## Cluster de calcul

Selon les périmètres et les puissances de calcul, nous distinguerons les [mésocentres](https://calcul.math.cnrs.fr/pages/mesocentres_en_france.html) régionaux, des infrastructures nationales.

* _Les mésocentres adaptés :_


    - [ISDM Calcul](https://meso-lr.umontpellier.fr/plateformes-hpc/); anciennement meso@LR; le mésocentre Montpellierain, hébergé au [`CINES`](https://www.cines.fr/),
    - [Cluster GenoToul](https://bioinfo.genotoul.fr/); structure de calcul nationale INRAE, hébergée à Toulouse. A privilégier si vous êtes personnel INRAE,
    - [Cluster CIRAD](https://www.southgreen.fr/content/cirad-cluster); membre de [Southgreen](https://www.southgreen.fr/), le cluster est assuré par l'ISDM. A privilégier si vous êtes personnel du CIRAD,
    - [Cluster IRD](https://www.southgreen.fr/content/ird-cluster); membre de [Southgreen](https://www.southgreen.fr/). A privilégier si vous êtes un personnel IRD.


* _Les infrastructures nationales :_


    - [Infrastructures nationales GENCI](https://genci.fr/) ; à privilégier pour les très gros calculs prévus de longues dates et déjà bien rodés,
    - [IFB Clusters](https://www.france-bioinformatique.fr/clusters-ifb/) ; réseau de clusters de l'IFB. Le cluster principal, `IFB-Core` est hébergé par l'[`IDRIS`](http://www.idris.fr/), membre du `GENCI`.


Si vous êtes personnel UM ou CNRS, il est préférable de privilégier l'ISDM, l'IFB ou les infrastructures nationales du GENCI, ou, à défaut, Genotoul (dont les limites par défaut sont assez élevées). En effet, Genotoul, à contrario de Southgreen (dont l'utilisation est strictement réservé au personnel CIRAD ou IRD), permet aux personnels non INRAE d'accéder à ses ressources.

A noter que Southgreen (avec les clusters IRD & CIRAD) et Genotoul sont membres de l'IFB; par ailleurs le cluster du CIRAD est hébergé par l'ISDM.

Selon votre institut de rattachement, il est possible que vous ayez accès également à d'autres mésocentres. Pour consulter la liste complète, voir [ici](https://calcul.math.cnrs.fr/pages/mesocentres_en_france.html).

Un comparatif des clusters IFB, ISDM et Genotoul est disponible [sur le nextcloud](https://cloud.isem-evolution.fr/nextcloud/index.php/s/BsTf3MKQ2EFy9zi) (le mot de passe est le même que pour l'Intranet de l'ISEM).


## Bigmems

Il y a 3 intérêts majeurs à utiliser des [bigmems à l'ISEM](bigmems/readme.md); celles-ci :

  1. donnent la possibilité d'avoir une totale autonomie sur la machine, y compris être `root` sur la machine,
  2. d'avoir de la puissance locale conséquente (RAM + CPU),
  3. d'avoir un stockage local assez rapide et conséquent.

En outre, leur utilisation est gratuite pour le personnel de l'institut et leurs collaborateurs.

Néanmoins, si ça ne vous convient pas, et que vous souhaitez tout de même externaliser cette partie (pour, par exemple, remplacer par du locatif/fonctionnement ou un service à la demande plus adapté), il convient de regarder, parmis les 3 critères donnés précédemment, lequel vous semble prioritaire.

En effet, un service identique à l'extérieur strictement équivalent, n'existe pas vraiment ou alors cela risque de vous coûter relativement cher.

### Alternative bigmem avec recherche autonomie

> _Avant tout, il faut se demander si l'option `root` est inévitable; en effet, de nombreux logiciels (la grande majorité) sont utilisables sans avoir besoin de privilèges élevés, et même dans le cas de dépendances systèmes, il est souvent possible de conteneuriser (voir [singularity](singularity.md)), ou de s'abstraire de cette problématique d'une autre manière (avec [`guix`](guix.md), par exemple)._

Une offre de type `cloud`, pour le 1er critère sera à privilégier. Il faudra alors chercher un gabarit de machine suffisamment conséquent et rajouter le stockage adapté à votre besoin.

Le [`CrOcc`](https://drocc.fr/crocc/), si disponible, reste l'option numéro 1 à privilégier. Si pas possible (finances, manque de ressources, etc), l'[`IFB Cloud`](https://www.france-bioinformatique.fr/cloud-ifb/) est également une très bonne option.

Le [`CrOcc`](https://drocc.fr/crocc/) est un cloud de type `IaaS` (mais quelques déploiement en mode `PaaS` sont aussi possibles ([`Onyxia`](https://www.onyxia.sh/), [`OKD`](https://www.okd.io/), ...)), co-localisé à Toulouse (Centre Clément Ader) & Montpellier (CINES). L'IFB Cloud, quant à lui, est un Cloud `IaaS` + `PaaS` avec un catalogue logiciels très important, correspondant aux besoins de notre communauté, localisé [sur de nombreux sites](https://biosphere.france-bioinformatique.fr/cloud/system_status).

Que ça soit avec [`Onyxia`](https://www.onyxia.sh/) côté [`CrOcc`](https://drocc.fr/crocc/) ou bien au travers d'une appliance dédiée côté [`IFB Cloud`](https://www.france-bioinformatique.fr/cloud-ifb/), le cloud reste une option très pratique pour les notebook Jupyter ou mettre en place un serveur rstudio/shiny (même si celà est souvent possible côté Cluster aussi).

> _(Pour une définition des termes `IaaS` et `PaaS` voir [ici](https://www.ovhcloud.com/fr/public-cloud/cloud-computing/iaas-paas-saas/))._

### Alternative bigmem avec puissance en local

Dans ce cas, notamment si le besoin `root` (1) n'est finalement pas si prépondérant, les clusters de calcul sont souvent une très bonne alternative. En effet, la plupart ont souvent une partition avec des machines étiquettées (`mem` (ISEM), `bigmem` (IFB-Core), noeuds `épais` (ISDM), queues `*smp*` )...

Pour les très gros besoins (>750Go RAM), les machines ISEM (sauf privées) ne seront pas suffisantes.

### Alternative bigmem stockage

Le besoin en stockage peut se découper en :

  - besoin d'un scratch / espace rapide,
  - besoin d'espace conséquent.

> Pour des volumes importants, il faudra vérifier les temps de transfert vers/depuis l'endroit où vous devez déposer/récupérer vos données.

Si vous avez besoin des deux, les bigmems de l'ISEM restent une très bonne option; néanmoins, si vous pouvez découper les espaces, vous devrez pouvoir alors utiliser à peu près n'importe quel cluster.

Pour le choix d'espaces rapides, il convient de vérifier comment sont accédés ceux-ci et quels choix technologiques ont été faits. Les stockages en RAM (`tmpfs` / `ramfs`) seront ainsi les plus performants, suivi des supports NVMe (PCIe) puis SSD connectés autrement (SATA, etc).

Concernant un stockage conséquent, il faut voir, avant tout, si vous pouvez monter votre stockage sur la plateforme désirée. Si ce n'est pas le cas, il est peu probable que vous puissiez stocker plusieurs To sur la plateforme ; et les temps de transferts risquent de vous impacter fortement.
Si votre stockage conséquent est déjà sur le même site (exemple : stockage ISDM / meso), vous bénéficierez d'un stockage proche de vos calculs pour des calculs sur le cluster ISDM/meso ou le [`CrOcc`](https://drocc.fr/crocc/).


## Calcul sur GPU

Le calcul sur GPU peut s'effectuer aussi bien sur un cluster classique disposant de noeuds GPU (souvent avec une partition dédiée ~`gpu`), que sur des machines dans un Cloud. Néanmoins, le fonctionnement par défaut des Clouds est souvent plus adapté, car il permet souvent de déployer des frameworks GPU facilement. Ainsi, la plupart des frameworks GPU sont disponibles sous forme de conteneur docker (et sont déployés très majoritairement de cette manière).

Cependant, utiliser un Cloud ne permet pas toujours de bénéficier pleinement et totalement de la puissance de la carte. En effet, `nvidia` propose des manières de partitionner les cartes pour les machines virtuelles (`MIG`, `vGPU`). Il faudra donc vérifier ce point et votre besoin avant de vous lancer sur un Cloud pour faire du GPU.

Par ailleurs, certains centre de calcul permettent tout de même de déployer des frameworks GPU, par leur service support ou de manière automatisée.


## Alternatives Rstudio

Le serveur `rstudio` MBB date et bien qu'il soit bien équipé en RAM, il n'a pas été prévu et configuré pour un usage massif & mutualisé.

Le [`CrOcc`](https://drocc.fr/crocc/) avec [`Onyxia`](https://www.onyxia.sh/), qui embarque le nécessaire pour faire tourner `Rstudio`, sera une très bonne alternative.
Le [`Cloud IFB`](https://www.france-bioinformatique.fr/cloud-ifb/) propose également une appliance [`Rstudio`](https://biosphere.france-bioinformatique.fr/catalogue/appliance/16/) ou par leur interface "_[`OnDemand`](https://ondemand.cluster.france-bioinformatique.fr/)_".

Voir aussi la [documentation de l'IFB à ce sujet](https://ifb-elixirfr.gitlab.io/cluster/doc/software/r/#migrate-from-rstudio-2-rrscript) pour une utilisation sur cluster.

Après mise au point de vos codes sous R (_avec éventuellement une étape de [conteneurisation](singularity.md), si, par exemple, vous avez besoin de dépendances systèmes_), vous devriez pouvoir calculer sur n'importe quel cluster présenté au début de cet article.


## Pour conclure

N'hésitez pas à [prendre contact](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) avec la plateforme, vis à vis de vos besoins.

Nous sommes aussi preneurs de vos retours d'expérience sur ces sujets.
