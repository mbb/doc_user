# Summary

Sommaire :

* [Cluster ISEM](readme.md)
  * [Introduction](cluster/cluster_intro.md)
  * [Connexion](cluster/cluster_connexion.md)
  * [Prise en main](cluster/cluster_prise_en_main.md)
  * [Soumission de job](cluster/cluster_soumission_de_job.md)
  * [transfert de fichiers](cluster/cluster_tranfert.md)
  * [Gérer ses jobs](cluster/cluster_gestion_des_jobs.md)
  * [Utiliser module](cluster/cluster_module.md)
<!--* [Description des services calcul, quoi utiliser ?](quoi_utiliser.md)-->
<!--* [Commencer sur MBB (Cluster et Site Web)](tutombb.md)-->
* [Commandes de base Linux](linux_base.md)
<!--* [Utilisation du cluster MBB ou ISE-M](cluster.md)-->
* [Les bigmems](bigmems/readme.md)
  * [Description du service](bigmems/bigmems.md)
  * [Prise en main d'une machine bigmem](bigmems/post-configuration.md)
  * [Exemple de sauvegarde des bigmems](bigmems/backup.md)
  * [Notions de sécurisation et mises à jour](bigmems/updates_and_security.md)
* [Sed/Grep/Awk](sedgrepawk.md)
* [Python](python.md)
* [R](r.md)
* [java](java.md)
* [perl](perl.md)
* [Singularity](singularity.md)
* [Guix](guix.md)
* [Rmpi](mpi-rmpi.md)
* [Optimisation en C++ (boost et MPI)](cpp.md)
* [Gnuplot](gnuplot.md)
* [Formations et Documents divers](bibliographie.md)
* [Gitlab MBB](gitlab.md)
* [Astuces - soucis divers](tips_and_bugs.md)
* [FAQ](FAQ.md)
* [Quoi utiliser ?](quoi_utiliser.md)
* [Les alternatives à l'extérieur](alternatives_exterieures.md)
