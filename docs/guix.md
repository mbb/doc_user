<!-- toc -->

# Guix

## Introduction

[Guix](https://guix.gnu.org/) est désormais disponible sur les cluster ISEM et MBB. Il s'agit d'un gestionnaire de paquet très évolué inspiré de [Nix](https://nixos.org/).

Guix permet d'améliorer sensiblement la reproductibilité des logiciels, notamment par une compilation bas niveau séparée de l'OS.

Guix stocke des checksums de chaque logiciel et fonctionne avec un système de liens symboliques permettant de diminuer l'espace utilisé.

Guix fonctionne (ou donne la possibilité de travailler) avec des systèmes dits de [`profils`](https://guix.gnu.org/blog/2019/guix-profiles-in-practice/), d'[`environnements`](https://guix.gnu.org/manual/en/html_node/Invoking-guix-environment.html) (désormais remplacés par la commande [`shell`](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html)) et de [`dérivations`](https://guix.gnu.org/manual/fr/html_node/Derivations.html), permettant de sanctuariser son déploiement d'applications, et, si besoin, de revenir à un autre état et de contrôler sa bonne installation. Des [`channels`](https://guix.gnu.org/manual/en/html_node/Channels.html) permettent de rajouter des sources logicielles externes (dépôts).

## Utilisation et installation de package

Pour utiliser guix sur le cluster, vous pouvez commencer par faire une recherche :

```bash
guix search font
# si "guix" ne fonctionne pas, essayez avec le chemin absolu
/usr/local/bin/guix search font
```

Pour avoir une aide sur une commande :

```bash
guix help <sous-commande guix>
# exemple :
guix help search
```

Pour installer un package :

```bash
guix install font-dejavu
# et pour spécifier un package avec une version
guix install perl@5.30.2
```

Ensuite, il faut mettre à jour son fichier `.bashrc` (si ce dernier ne contient pas encore ces informations) :

```bash
echo 'GUIX_PROFILE="$HOME/.guix-profile"' >> ~/.bashrc
echo '. "$GUIX_PROFILE/etc/profile"' >> ~/.bashrc

# si besoin
# pour les locales
echo 'export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"' >> ~/.bashrc
# pour l'auto-completion
echo '. /var/guix/profiles/per-user/root/current-guix/etc/bash_completion.d/guix' >> ~/.bashrc
source ~/.bashrc
```

```bash
whereis perl
perl -V
  > Summary of my perl5 (revision 5 version 30 subversion 2) configuration:
  > ...
# si la version affichée n'est pas la bonne :
$HOME/.guix-profile/bin/perl -V
```

> **Note** `$HOME/.guix-profile` contient le profil par défaut; c'est votre 1er profil. Chaque nouvelle installation de package crée un nouvelle génération de profil. Utilisez `guix --list-generations` afin de lister les différentes générations de votre profil.

Mettre à jour :

```bash
# mise à jour de guix
guix pull

# puis mise à jour des packages
guix package -u
# ou
guix upgrade
```

Voir la liste des packages installés :

```bash
guix package -I
```

Liste des packages disponibles :

```bash
guix package -A
```

Plus d'informations sur un package :

```bash
guix show perl@5.30.2
```

Supprimer un package :

```bash
guix -r <nom du package>
```

Pour plus d'informations sur la commande package : `guix help package`.

Il est possible d'exporter la description de vos packages au sein d'un fichier `manifest.scm` :

```bash
guix package --export-manifest > manifest.scm
```

### Chemins et environnements Guix

Si le chemin vers le binaire guix pose problème, il faut alors vérifier où sont les binaires :

```bash
exec $SHELL
whereis perl
```

Normalement, si vous avez rajouté ce qu'il faut dans votre fichier `~/.bashrc`, alors le chemin pointant vers les binaires guix devrait se charger.

Un chemin commançant par `/share/gnu/store/...` ou par `${HOME}/.guix-profile/bin/` correspondra ainsi à votre dernière installation.

Dans ce cas, vous pouvez appeler, au besoin, directement le binaire par son chemin absolu :

```bash
${HOME}/.guix-profile/bin/perl -V
```

> **Tag** Le module déprécié `deprecated/python/3.8` utilisait guix.

## Recherche avancée

Par défaut la recherche guix va chercher dans tous les champs ce qui contient votre chaine de caractère recherchée.

Ainsi `guix search python` va renvoyer beaucoup de résultats.

Pour éviter ça, on va utiliser `recsel` :

```bash
# on installe recutils ("guix install" est un alias de "guix package -i")
guix package -i recutils
guix search python |recsel -p name,version -e 'name ~ "^python$"'
```

## Gestion des profils

Il est possible d'avoir plusieurs profils qui isoleront nos différents environnements logiciel.

Pour lister les profils existants :

```bash
guix package --list-profiles
```

Pour connaître votre profil actuel, il suffit d'afficher la variable d'environnement `$GUIX_PROFILE` :

```bash
echo $GUIX_PROFILE
```

Voir [cet article](https://guix.gnu.org/blog/2019/guix-profiles-in-practice/) pour voir comment gérer vos profils.

## Environnements éphémères

Guix permet de gérer des environnements éphémères par l'utilisation de [`guix shell`](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html). A la manière des `virtualenv` en python, ils permettent d'isoler ce qu'on fait vis à vis des profils guix existant et même du système.

`guix shell` crée une variable d'environnement `$GUIX_ENVIRONMENT` à son lancement permettant de savoir si guix shell a été utilisé ou pas.

```bash
echo $GUIX_ENVIRONMENT
```

Un changement de répertoire et `unset GUIX_ENVIRONMENT`, vous permettra de revenir à l'état précédent.

Vous pouvez créer un conteneur avec guix shell et guix pack; pour cela, voir la vidéo tout en bas de cet article.

### Enrichir le catalogue logiciel

`Guix` peut paraitre assez pauvre en terme de quantités de packages. C'est effectivement le cas si l'on compare, par exemple, à `Nix` ou à `conda`. Néanmoins, par son côté très libre (logiciel GNU) et reproductible, avec ses capacités avancées de création de package ou conteneurs font de cet outil un incontournable sur un cluster.

Par défaut, voici à quoi ressemblerait votre fichier `channels.scm` :

```scheme
(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "a0178d34f582b50e9bdbb0403943129ae5b560ff")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))
```

Ce résultat est obtenu à partir de la commande `guix describe -f channels`. `git.savannah.gnu.org` correspond au channel par défaut. Il n'est pas nécessaire de le spécifier ainsi, car il est déjà connu (voir ci-dessous).

Il est possible d'augmenter grandement le nombre de packages en rajoutant d'autres channels :

```bash
if [ ! d ~/.config/guix ]; then
  mkdir -p ~/.config/guix
fi

cat << EOF >> ~/.config/guix/channels.scm
(list %default-guix-channel
      (channel
        (name 'guix-science)
        (url "https://github.com/guix-science/guix-science")
        (introduction
         (make-channel-introduction
          "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
          (openpgp-fingerprint
           "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
      (channel
        (name 'guix-cran)
        (url "https://github.com/guix-science/guix-cran"))
      (channel
        (name 'gricad-guix-packages)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
      (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc")))
EOF

# ensuite on lance la reconstruction des définitions de packages disponibles
# ... vous aurez le temps d'aller boire un café
guix pull
# par défaut, guix pull va chercher dans le profil ~/.config/guix/current
# pour voir la liste des dérivations construites
guix pull --list-generations


# pour supprimer toutes les dérivations plus vieilles de 2 mois
guix package --delete-generations=2m
# pour ne conserver que la dernière génération sur le profil ~/.config/guix/current
guix pull -p ~/.config/guix/current --delete-generations

guix describe -p ~/.config/guix/current

# puis on peut installer conda
guix search conda
guix install conda
```

> **Info** guix tente de concilier tous les packages installés depuis les dépôts comme le ferait un `apt upgrade -y` lors d'un `guix pull`. Or, il se peut que vous ayez besoin d'une version récente d'un paquet présente uniquement sur l'un des channels. Dans ce cas, il est plus sensé de ne faire apparaître que ce dépôt dans le fichier `channels.scm` en plus du dépôt guix standard.

Ainsi, si vous souhaitez uniquement rajouter un seul dépôt, vous pouvez configurer votre fichier channel ainsi :

```scheme
;; Add variant packages to those Guix provides.
(cons (channel
        (name 'variant-packages)
        (url "https://example.org/variant-packages.git"))
      %default-channels)
```

> **Note** Avoir peu de dépôts et de packages permet en général d'améliorer la portabilité en diminuant la taille du profil.

`%default-channels` correspond au channel par défaut. Voir la [doc](https://guix.gnu.org/manual/en/html_node/Specifying-Additional-Channels.html) à ce sujet.

Dès que vous créez un nouveau profil, il faudra mettre à jour votre binaire guix avec celui qui se trouve dans votre profil, ou modifier votre variable d'environnement `$GUIX_PROFILE` (`~/.bashrc`) ou bien utiliser `$GUIX_EXTRA_PROFILES`.

Ainsi, le fichier `/usr/local/bin/guix` cité au départ ne doit être utilisé qu'au début avec le profil par défaut. Vous pouvez l'écraser ainsi :

```bash
if [ ! -d ~/bin ]; then
  mkdir ~/bin
fi
# pour un profil correspondant à ~/.config/guix/current :
ln -sf ~/.config/guix/current/bin/guix ~/bin/guix
echo 'export PATH=~/bin:$PATH' >> ~/.bashrc
source ~/.bashrc
```

#### Gestion de conflits

> **Warning** Si, comme moi, vous vous retrouvez dans un état où `guix pull` ne fonctionne plus, ni `guix pull --allow-downgrades`, il vous faudra enlever le ou les dépôt(s) problématique(s), jusque au moment où ça arrive à compiler correctement. En effet, certains dépôts sont parfois basés sur des versionnes anciennes de guix, et l'utilisation du dépôt `guix-past` ne permet parfois pas de remédier au problème avec des logiciels récents installés (comparable à de la résolution de conflits dans `git`).

Lorsque vous avez un environnement qui vous convient, vous pouvez tout figer dans un nouveau fichier channels que vous réutiliserez plus tard :

```bash
guix describe -f channels >> `date +%Y%m%d`_channels.scm

# puis pour mettre à jour ultérieurement depuis ces dépôts
# si la date est au 01.12.2022
guix pull -C 20221201_channels.scm
guix package -u
```

## Aller plus loin

### Utilisation avancée

Si vous avez bien suivi, il est possible de figer son environnement logiciel et les dépôts permettant de parvenir à cet état. Pour cela, 2 fichiers sont utilisés  : `manifest.scm`, listant les logiciels avec leur version, et `channels.scm` listant les dépôts.
Si vous versionnez ces fichiers dans un dépôt avec vos scripts/codes, vous pourrez rendre votre environnement portable et reproductible.
Je vous conseille de visioner la vidéo citée plus bas pour y parvenir.

### Cafés Guix

Les [cafés guix](https://hpc.guix.info/events/2022/caf%C3%A9-guix/) sont des moments d'échange mensuels et informels autour de Guix, organisé par le monde de la recherche.

- [Vidéo très intéressante de prise en main de Guix de Pierre-Antoine Bouttier](https://hpc.guix.info/static/videos/caf%C3%A9-guix/bouttier-20221025.mp4) avec du `guix shell`, des `manifest.scm` et la création d'image Singularity !

### Documentation officielle

Les [cookbooks](https://guix.gnu.org/fr/cookbook/) sont un point d'entrée intéressant (moins formalisés et plus lisibles que le manuel)...
