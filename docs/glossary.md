# Glossaire

## SLURM
Gestionnaire de jobs OpenSource qui remplace désormais SGE sur le cluster ISEM.

## SGE
Sun Grid Engine. C'est un gestionnaire de batch très utilisé/déployé. SGE fut créé (à partir de Codine/GRD) puis porté par Sun sur Solaris, en 2000, suite à l'acquisition de l'entreprise Gridware. Suite au rachat de Sun par Oracle, SGE devient Oracle Grid Engine. Racheté depuis par Univa. La version propriétaire de SGE est donc désormais Univa Grid Engine.

## OGS
Open GridScheduler est l'un des deux forks (avec SoG) OpenSource du projet SGE OpenSource, fermé en 2010. Il n'a pas été mis à jour depuis 2012. C'est cette version qui est incluse dans Roll SGE de RocksClusters.

## SoG
Son Of GridEngine est l'un des deux forks (avec OGS) OpenSource du projet SGE OpenSource, fermé en 2010. Il reste encore assez actif. Ce code représente certainement l'avenir (s'il y en a encore un) de SGE version OpenSource.

## Univa GridEngine
Univa est une société qui a racheté le code propriétaire Oracle de SGE. Certains des principaux développeurs de SGE ont intégré Univa.

## RocksCluster
RocksCLuster, développé par le "San Diego Supercomputer Center" à l'UCSD (Université de Californie, San Diego) est une distribution Linux dérivée de CentOS dont le but est de donner la possibilité d'installer un cluster de calcul clé en main, de manière simple. Les surcouches utilisées pour cette distribution clé en main sont une suite de codes en python `rocks`, une base mysql, anaconda/kickstart, apache, 411, NFS et autofs. Des packages "rolls" sont proposés en standard pour compléter la distribution (ganglia, SGE, hpc, bio, perl, python, KVM, ZFS...).

## Roll
La distribution RocksCluster est découpée en _Rolls_. Ainsi, cette distribution est modulable. Il est par exemple possible de rajouter un Roll _SL_ afin de passer sur une distribution ScientificLinux et l'utiliser à la place du roll _OS_. D'autres _Rolls_ permettent de déployer un ensemble de packages préconfigurés.

## Modules
*Modules* ou plus exactement _Environment Modules_ permet de gérer dynamiquement les variables d'environnement de vos utilisateurs. Ainsi, un utilisateur peut exécuter plusieurs versions d'un même logiciel en changeant simplement de contexte par *module*.

## Environnement parallèle
Un Environnement parallèle dans SGE permet de définir la manière dont les jobs vont être exécutés dans le cadre d'applications parallélisées (multithreads, MPI...).

## Queue
Une queue permet à un gestionnaire de batch de découper un cluster en plusieurs sous-grappes fonctionnelles/matérielles. Elle peut être explicitement déclarée à la soumission du job ou non (le job sera lancé où il peut en fonction des droits des disponibilités...).

## Backfilling
Dans SGE, ce procédé permet de remplir au mieux les ressources disponibles en démarrant des jobs courts, à faible priorité, alors que d'autres jobs - plus importants (en priorité/temps) - sont en attente (d'une _Advance Reservation_ par exemple).

## Advance Reservation
Ce procédé permet à SGE de réserver des ressources pour un utilisateur sur une période donnée.

## MPI
*Message Passing Interface* est une norme de communication définissant une bibliothèque de fonctions utilisables en C/C++/Fortran. Il y a deux implémentations principales de MPI: *OpenMPI* et *MPICH*, tous les 2 sont _OpenSource_. Les communications MPI s'appuyant sur le réseau, il est important que celui-ci soit performant. ([source:wikipedia](https://fr.wikipedia.org/wiki/Message_Passing_Interface))

## OpenMP
Interface de programmation pour faire des applications multithreads (multicoeurs) en C/C++/Fortran. Des directives (ou pramas) permettent d'indiquer dans le code où paralléliser les tâches (généralement devant les boucles). La version 4 de OpenMP est compatible GPU mais n'est pas encore vraiment disponible ([gcc5](https://gcc.gnu.org/gcc-5/changes.html))
([Exemples OpenMP4](http://openmp.org/mp-documents/OpenMP4.0.0.Examples.pdf))
 A ne pas confondre avec OpenMPI qui s'utilise pour des applications MPI.

## Partition
Voir [queue](#queue).

## Gestionnaire de batch
Un gestionnaire de batch (ou _Batch Scheduler_) est un logiciel permettant d'allouer des ressources en fonction des utilisateurs, de leurs jobs et des priorités ( = [jobs manager/scheduler](https://en.wikipedia.org/wiki/Job_scheduler) + resources manager) sur un cluster.

## Gestionnaire de jobs
Voir gestionnaire de batch
