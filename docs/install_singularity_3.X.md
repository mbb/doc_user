<!-- TOC -->
- [1. The Install](#1-the-install)
- [2. Some checking](#2-some-checking)
- [3. Modulefiles](#3-modulefiles)
<!-- /TOC -->

*Huge thanks to Justin Cook from Sylabs who helped me to install Singularity 3.1*

My Cluster is using CentOS 6.6

Shared applications are stored in `/share/apps/bin` while shared libraries are stored in `/share/apps/lib`.
A `localstatedir` is already present in a local scratch directory.

Here are the steps I used to install Singularity 3.1 on Centos 6 on a NFS share directory.

# 1. The Install

```bash
# ssh on a node and going in the /tmp directory to work in a clean way
qrsh
cd /tmp

# installing libuuid depedency from rpm in a NFS share location
# otherwise, "yum --enablerepo=epel install libuuid-dev openssl-devel"
# would have done the job.
mkdir -p /share/apps/lib/libuuid/2.17.2
mkdir libuuid
wget https://www.rpmfind.net/linux/centos/6.10/os/x86_64/Packages/libuuid-2.17.2-12.28.el6_9.2.x86_64.rpm
wget https://www.rpmfind.net/linux/centos/6.10/os/x86_64/Packages/libuuid-devel-2.17.2-12.28.el6_9.2.x86_64.rpm
mv libuuid-* libuuid
cd $_
rpm2cpio libuuid-devel-2.17.2-12.28.el6_9.2.x86_64.rpm | cpio -idmv
rpm2cpio libuuid-2.17.2-12.28.el6_9.2.x86_64.rpm | cpio -idmv
mv usr/* /share/apps/lib/libuuid/2.17.2/
mv lib64/* /share/apps/lib/libuuid/2.17.2/lib64/

# installing openssl-devel depedency in a NFS share location
mkdir openssl
mkdir -p /share/apps/lib/openssl/1.0.1e
wget http://rpmfind.net/linux/centos/6.10/os/x86_64/Packages/openssl-devel-1.0.1e-57.el6.x86_64.rpm
wget http://mirror.centos.org/centos/6/os/x86_64/Packages/openssl-1.0.1e-57.el6.x86_64.rpm
mv openssl-* openssl
cd $_
rpm2cpio openssl-devel-1.0.1e-57.el6.x86_64.rpm | cpio -idmv
rpm2cpio openssl-1.0.1e-57.el6.x86_64.rpm | cpio -idmv
mv usr/* /share/apps/lib/openssl/1.0.1e
mv etc /share/apps/lib/openssl/1.0.1e


# installing go depedency in a NFS shared directory
mkdir -p /share/apps/bin/golang/1.11
export VERSION=1.11 OS=linux ARCH=amd64 && \
    wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \
    sudo tar -C /share/apps/bin/golang/1.11 -xzvf go$VERSION.$OS-$ARCH.tar.gz && \
    rm go$VERSION.$OS-$ARCH.tar.gz
echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \
    echo 'export PATH=/share/apps/bin/golang/1.11/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc

# as go is not install in a standard location (ie: /usr/local/go), we need to specify the $GOROOT
echo 'export GOROOT=/share/apps/bin/golang/1.11/go' >> ~/.bashrc
source ~/.bashrc

# following is optionnal (it is just to have a working `go get` with mercurial)
# I did not install it in NFS shared directory, because, IMHO, it is unnecessary for my users
yum --enablerepo=epel install hg

# getting last release of singularity from the git master branch due to issues
# https://github.com/sylabs/singularity/issues/2904
# https://github.com/sylabs/singularity/issues/2543
# need to move to another location due to a Go weird behaviour
mkdir -p /share/apps/bin/singularity/3.1
mkdir -p $GOPATH/src/github.com/sylabs
cd $_
git clone https://github.com/sylabs/singularity.git
cd singularity
# VERSION file needed by mconfig is only present in tarball/releases.
echo "3.1.1" > VERSION
# my scratch is `/export/scrach`, so localstatedir will be /export/scrach/singularity/mnt/...
CFLAGS="-I/share/apps/lib/openssl/1.1.0b/include -I/share/apps/lib/libuuid/2.17.2/include" \
    CGO_CFLAGS="-I/share/apps/lib/openssl/1.1.0b/include -I/share/apps/lib/libuuid/2.17.2/include" \
    LDFLAGS="-L/share/apps/lib/openssl/1.1.0b/lib/ -L/share/apps/lib/libuuid/2.17.2/lib64" \
    CGO_LDFLAGS="-L/share/apps/lib/openssl/1.1.0b/lib/ -L/share/apps/lib/libuuid/2.17.2/lib64" \
    ./mconfig --prefix=/share/apps/bin/singularity/3.1 --localstatedir=/export/scrach
make -C builddir
make -C builddir install
```

And voilà !

# 2. Some checking

We need to check now if everything is fine:

```bash
# logging to another node...
cd $GOPATH/src/github.com/sylabs/singularity
make -C builddir test &> tests.txt
cat tests.txt
# ok there are some failures, but coverage seems to be quite good enough

cd /tmp
/share/apps/bin/singularity/3.1/bin/singularity shell docker://ubuntu:16.04
Singularity ubuntu_16.04.sif:/tmp> echo $SINGULARITY_NAME
ubuntu_16.04.sif
Singularity ubuntu_16.04.sif:/tmp> exit
exit
```

# 3. Modulefiles


Nice ! Now, let's creating some modulefiles (which I could have created earlier to avoid some big command lines...)


OpenSSL
```
#%Module######################################################################
#
# openssl 1.0.1e modulefile 20190315
# from http://rpmfind.net/linux/centos/6.10/os/x86_64/Packages/openssl-devel-1.0.1e-57.el6.x86_64.rpm

proc ModulesHelp { } {
puts stderr "This modulefile defines the library paths and"
puts stderr "include paths needed to use OpenSSL-devel 1.0.1e"
puts stderr "The program OpenSSL"
puts stderr "is added to PATH."
}


set is_module_rm [module-info mode remove]

set OPENSSL_LEVEL 1.0.1
set OPENSSL_SUBVER e
set OPENSSL_CURPATH /share/apps/lib/openssl

prepend-path LD_LIBRARY_PATH  $OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/lib64
prepend-path MANPATH          $OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/share/man

prepend-path --delim " " C_INCLUDE_PATH         -I$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/include
prepend-path --delim " " CPLUS_INCLUDE_PATH     -I$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/include

prepend-path --delim " " CGO_LDFLAGS      -L$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/lib64
prepend-path --delim " " CGO_CFLAGS       -I$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/include

prepend-path --delim " " LDFLAGS          -L$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/lib64
prepend-path --delim " " CFLAGS           -I$OPENSSL_CURPATH/$OPENSSL_LEVEL$OPENSSL_SUBVER/include


append-path  PE_PRODUCT_LIST  OPENSSL-$OPENSSL_LEVEL$OPENSSL_SUBVER
```

Libuuid
```
#%Module######################################################################
#
# libuuid 2.17.1 modulefile 20190319
# from http://rpmfind.net/linux/centos/6.10/os/x86_64/Packages/..
# libuuid-2.17.2-12.28.el6_9.2.x86_64.rpm  libuuid-devel-2.17.2-12.28.el6_9.2.x86_64.rpm.

proc ModulesHelp { } {
puts stderr "This modulefile defines the library paths and"
puts stderr "include paths needed to use libuuid-devel 2.17.2"
puts stderr "The library libuuid-devel"
puts stderr "is added to PATH."
}


set is_module_rm [module-info mode remove]

set LIBUUID_LEVEL 2.17.2
set LIBUUID_CURPATH /share/apps/lib/libuuid

prepend-path LD_LIBRARY_PATH  $LIBUUID_CURPATH/$LIBUUID_LEVEL/lib64
prepend-path MANPATH          $LIBUUID_CURPATH/$LIBUUID_LEVEL/share/man

prepend-path      --delim " " C_INCLUDE_PATH	     -I$LIBUUID_CURPATH/$LIBUUID_LEVEL/include
prepend-path      --delim " " CPLUS_INCLUDE_PATH     -I$LIBUUID_CURPATH/$LIBUUID_LEVEL/include

prepend-path      --delim " " CPPFLAGS  -I$LIBUUID_CURPATH/$LIBUUID_LEVEL/include
prepend-path      --delim " " CFLAGS    -I$LIBUUID_CURPATH/$LIBUUID_LEVEL/include
prepend-path      --delim " " LDFLAGS   -L$LIBUUID_CURPATH/$LIBUUID_LEVEL/lib64


prepend-path      --delim " " CGO_LDFLAGS      -L$LIBUUID_CURPATH/$LIBUUID_LEVEL/lib64
prepend-path      --delim " " CGO_CFLAGS       -I$LIBUUID_CURPATH/$LIBUUID_LEVEL/include


append-path  PE_PRODUCT_LIST  LIBUUID-$LIBUUID_LEVEL
```

Singularity 3.1
```
#%Module######################################################################
#
# singularity 3.1 modulefile
#

proc ModulesHelp { } {
puts stderr "This modulefile defines the library paths and"
puts stderr "include paths needed to use singularity 3.1"
puts stderr "The program singularity 3.1"
puts stderr "is added to PATH."
}

set is_module_rm [module-info mode remove]

unsetenv LD_LIBRARY_PATH
set SING_LEVEL 3.1
set SING_CURPATH /share/apps/bin/singularity/$SING_LEVEL/

prepend-path PATH       $SING_CURPATH/bin/
prepend-path LD_LIBRARY_PATH  $SING_CURPATH/lib/
prepend-path MANPATH      $SING_CURPATH/share/man

append-path  PE_PRODUCT_LIST  Singularity.$SING_LEVEL
```

Go 1.11
```
#%Module######################################################################
#
# golang 1.11 20190320
#

proc ModulesHelp { } {
puts stderr "This modulefile defines the library paths and"
puts stderr "include paths needed to use Go (golang) version 1.11"
puts stderr "The program Go 1.11"
puts stderr "is added to PATH."
}

set is_module_rm [module-info mode remove]

unsetenv LD_LIBRARY_PATH
set GO_LEVEL 1.11
set GO_CURPATH /share/apps/bin/golang/$GO_LEVEL/go

prepend-path PATH       $GO_CURPATH/bin/
prepend-path LD_LIBRARY_PATH  $GO_CURPATH/lib/
prepend-path MANPATH      $GO_CURPATH/share/man
setenv GOPATH	~/go
setenv GOROOT	$GO_CURPATH

append-path  PE_PRODUCT_LIST  Golang.$GO_LEVEL
```
