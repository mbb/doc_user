# Quoi Utiliser ?

Si vous êtes sur cette documentation, c'est que vous vous demander quel service de la plateforme serait le plus adapté à votre problème.

## Introduction

En terme de calcul, la plateforme dispose de machines GPU, de machines que nous nommons `bigmems`, d'un cluster de calcul destiné au personnel `ISEM`, d'une machine serveur pour le logiciel rstudio et de 2 machines GPU disponibles en réservation. Le périmètre par défaut de nos services est le [LabEx CeMEB](http://www.labex-cemeb.org/), à l'exception du cluster ISEM.
Dans tous les cas, le préalable est de faire [une demande de compte MBB](https://kimura.univ-montp2.fr/aide/index.php?a=add&catid=20) pour accéder à nos services.

Je ne vais pas décrire ici tous nos services, mais uniquement me concentrer sur l'aspect calcul. Sachez juste que notre plateforme propose également de l'aide au développement logiciel (ISEM uniquement), un appui en bioinformatique [\*](#note_bioinfo) et une forge gitlab (+ du stockage et de l'hébergement Web pour le personnel ISEM).

<a id='note_bioinfo'></a> _\* Note : cet appui se concrétise par un appui à la mise en place et au développement de workflows et par une puissance de calcul auxiliaire avec des machines dédiées (autrefois : service `galaxy`...)_

## Description des services hébergés

### Serveur Rstudio

**Le serveur rstudio**, bien que relativement puissant, n'a pas vocation a accueillir tous vos calculs sous `R`, bien au contraire. Le but est justement d'ajuster et tester vos scripts `R`, avant de les soumettre sur un environnement plus large.

### Machines GPU

**Les machines GPU** permettent d'accomplir des tâches qu'il pourrait être compliqué d'accomplir sur clusters ou machines `bigmems`, sur CPU. En effet, pour tous les calcul ayant un lien avec le traitement / reconnaissance d'images, l'IA, (etc), le calcul sur GPU est plus adapté. Cependant, cela nécessite que la problématique soit adaptée et que le code utilisé soit compatible (cuda...). Ces machines utilisent l'envrionnement de conteneurisation `docker` et elles sont accessibles après réservation sur notre plateforme de réservation `GRR` (merci de faire [un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) pour y accéder la 1ère fois).

Les machines sont néanmoins assez anciennes et il est préférable de vous réorienter vers les alternatives proposées par d'autres mésocentres, disposant de machines plus performantes. Voir la page des [alternatives](alternatives_exterieures.md) pour plus d'inforamtions.


### Cluster ISEM

> **Attention, pour des raisons d'encombrement et de gestion, il est prévu d'arrêter le cluster ISEM dans un avenir proche. Merci de vous reporter à la page des [alternatives](alternatives_exterieures.md) pour travailler plus sereinement.**


**Le cluster de calcul de l'ISE-M** est réservé au personnel de l'ISE-M. Sa création précède l'existence du _LabEx_. Ce dernier dispose de plusieurs queues dont certaines sont privées. Les files d'attente (`partitions`) sont :

```bash
sinfo --hide --noheader -r
> long ...
> mem ...
> small ...
```

Les machines dans la queue `mem` disposent de plus de mémoire. La queue `small` est limitée, pour l'exécution des jobs, à une durée de 7 jours.

La queue `long` sera donc certainement votre queue par défaut sur ce cluster.

Vous pouvez lister l'appartenance des machines et leurs caractéristiques par queue avec :

```bash
scontrol show partition <partition>
```

Si jamais la queue `mem` ne suffit pas à faire tourner vos travaux, vous avez alors la possibilité de réserver une machine `bigmems`.

### Cluster MBB

**Le cluster de calcul MBB** était accessible à toute personne faisant partie du [LabEx CeMEB](http://www.labex-cemeb.org/).

Ce dernier n'est désormais plus disponible.

## Machines Bigmems

MBB possède 3 machines 64 coeurs et 512Go de RAM accessibles après demande de réservation (plateforme de réservation `GRR`; merci de faire [un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) pour y accéder la 1ère fois). Les machines ne sont pas connectées au cluster vous permettant d'avoir :

1. un accès complet et total sur la machine le temps de la réservation,
2. de pouvoir lancer directement vos jobs sur la machine sans passer par un _Job Scheduler_ comme `SGE` ou `SLURM`.

Ces machines sont installées par défaut sous Ubuntu 18.04. Les machines sont réinstallées entre chaque réservation, il vous faut donc prévoir le temps de transfert de vos données dans la durée totale de la réservation.
La durée maximum de réservation est de 3 semaines consécutives par projet, renouvelable 1 fois par période glissante de 1 trimestre.

Ces machines disposent d'un stockage interne de 3 à 6To et nous pouvons éventuellement connecter/monter des espaces tierces [sur demande](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html).

Si ces machines ne suffisent pas alors nous vous conseillons de vous tourner vers le mésocentre local (`meso@lr`).

## Contraintes d'accès et politique de la plateforme

La plateforme n'assure aucune sécurité ou sauvegarde sur vos données (sauf pour le service stockage de l'`ISEM`).
Nos services sont soumis aux chartes de nos tutelles que sont notre organisme d'accueil, l'UM, le CNRS et pour le réseau, Renater.

1. https://www.umontpellier.fr/wp-content/uploads/2014/07/CHARTE-USAGE-SI-UMontpellier.pdf
2. https://www.renater.fr/documentation/chartes/

Si le travail n'a bénéficié que d'un support purement ISEM fourni par ISI, tel que le Cluster ISEM, la citation est :

```text
"This work benefited from the ISEM computing facility, at ISI service."
```

Pour tout travail associé au LabEx CeMEB utilisant les service de la plateforme calcul et bioinformatique de Montpellier Bioinformatics Biodiversity, merci d'inclure cette formule dans vos publications :

```
[Replace_with_your_project_name] benefited from the Montpellier Bioinformatics Biodiversity platform supported by the LabEx CeMEB, an ANR "Investissements d'avenir" program (ANR-10-LABX-04-01).
```
