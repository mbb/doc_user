# Backup des bigmems

<!-- toc -->

Pour rappel, les machines ne sont pas sauvegardées (!), c'est à vous de le faire, en fonction de vos espaces de stockage (si vous en avez...) !

Cette documentation se penche donc sur un cas d'utilisation où vous auriez acheté du stockage ISDM en mode SFTP.

Admettons que le stockage conséquent à sauvegarder soit dans `/bigvol/<user>/backup`. En effet, les home sont conservées à cet endroit sur la plupart de ces machines.

On crée par exemple un fichier `test.txt` dans notre sous-dossier backup pour nos tests :

```bash
mkdir /bigvol/<user>/backup
echo -e  "test test test\n ok ok ok" > /bigvol/<user>/backup
```



## Restic

Pour faire nos sauvegardes, nous allons utiliser [`restic`](https://restic.net/).

Ce dernier est très puissant (compatible S3, avec compression, déduplication, ...) et assez simple d'utilisation.

La documentation est basé essentiellement sur ces 2 ressources :

  - [https://ressources.labomedia.org/restic_utilitaire_de_sauvegarde](https://ressources.labomedia.org/restic_utilitaire_de_sauvegarde)
  - [https://www.ivonne-y-will.fr/articles/restic-bases/](https://www.ivonne-y-will.fr/articles/restic-bases/)

## Installation et initialisation

Nous allons faire tout ce qui suit en root, même s'il est possible et même recommandé de créer un utilisateur dédié.

L'installation se fait simplement, en utilisant les dépôts officiels de la distribution :

```bash
apt install -y restic
```

Pour l'initialisation, c'est également assez simple. On va d'abord s'assurer qu'on peut se connecter sur le système de stockage distant, sans mot de passe (les valeurs entre chevrons sont à remplacer) :

```bash
# si vous n'avez pas encore de clé publique sur la bigmem
ssh-keygen -t ed25519
# puis on copie la clé sur le système distant
ssh-copy-id <user>@<host>
```

> Pensez à remplacer ici et dans la suite de cette documentation les valeurs entre chevrons (symboles inférieurs/supérieurs : `<`_valeur-à-remplacer_`>`)

On se crée ensuite un fichier ssh_config sur la bigmem, s'il n'existe pas encore :

```bash
cat <<EOF >>~/.ssh/config
Host isdmbckp
    Hostname <host>
        User <user>
        ServerAliveInterval 60
        ServerAliveCountMax 240
EOF
```

On crée un sous-dossier `~/workdir/restic` sur la machine distante, puis :


```bash
restic --repo sftp:<user>@isdmbckp:~/workdir/restic init
```

On choisit avec un soin un mot de passe, qu'on conserve méticuleusement.


## Premiers backups et configuration

```bash
export RESTIC_PASSWORD=RESTIC_PASSWORD
restic -r sftp:isdmbckp:workdir/restic --verbose backup --tag firstcommit /bigvol/<user>/backup/
```

On parcourt nos snapshots :

```bash
# on liste les snapshots
restic snapshots -r sftp:isdmbckp:workdir/restic
# on liste le contenu du snapshot
restic ls <snapshot_ID> -r sftp:isdmbckp:workdir/restic
```

On teste la restauration :

```bash
# on tente un montage du snapshot en local
mkdir /mnt/restic
restic mount -r sftp:isdmbckp:workdir/restic /mnt/restic
# dans un autre terminal / tmux ...
ls /mnt/restic/snapshots/<date>
# on peut ensuite copier/ récupérer ce qu'on veut
# puis Ctrl+C sur premier terminal lorsque opération de récupération terminée
# restauration (attention à l'espace utilisé localement)
restic restore <snapshot_ID> -r sftp:isdmbckp:workdir/restic --target /tmp
# pour filtrer le contenu à restaurer, on utilisera le flag `--include` combiné au `--restore`, 
# ou bien directement `--dump` sur un snapshot afin d'afficher directement le contenu d'un fichier

# exemple d'un dump après création et sauvegarde d'un fichier test.txt à la racine du sous-dossier backup
restic dump <snapshot_ID> -r sftp:isdmbckp:workdir/restic /bigvol/<user>/backup/test.txt
```

Plus d'informations sur la restauration sur [la documentation officielle](https://restic.readthedocs.io/en/latest/050_restore.html).

Si ok, on va pouvoir scripter ça et le mettre en cron.

Dans le script suivant, j'ai configuré une rétention des 3 derniers backups hebdomadaires + 3 derniers backups mensuels.

```bash
echo '#!/bin/bash

# nécessaire si appelé depuis un envionnement cron
export PATH=$PATH:/usr/bin/:/usr/sbin:/usr/local/bin:/usr/local/sbin

REPOSITORY="sftp:isdmbckp:workdir/restic"
DATE=$(date +"%Y%m%d")
export RESTIC_PASSWORD=<votre mot de passe>

if [ ! -d /var/log/restic/ ]; then
  mkdir /var/log/restic/
fi

LOG=/var/log/restic/${DATE}.log

for d in /bigvol/*/backup; do
  if [ -d ${d} ]; then
    restic --verbose forget --path ${d} -r ${REPOSITORY} --keep-weekly 3 --keep-monthly 3 --prune >> ${LOG}
    restic --verbose backup --tag ${DATE}_${HOSTNAME}_$(dirname ${d:8}) -r ${REPOSITORY} ${d} >> ${LOG}
  fi
done

restic stats -r ${REPOSITORY} >> ${LOG}' > /usr/local/sbin/isdmbckp.sh

chmod 750 /usr/local/sbin/isdmbckp.sh
```

On a créé un script qui nous permet de taguer chaque snapshot de la machine avec la date du jour, le hostname de la machine + le nom du sous-dossier de l'utilisateur. On a donc un snapshot par utilisateur, suivi d'un nettoyage puis un affichage des statistiques. Le résultat étant consultable dans un fichier de log à la date du jour, dans `/var/log/restic`.

Pensez néanmoins à remplacer `<votre mot de passe>` et le chemin vers votre dépôt distant (valeur `REPOSITORY` dans le script).

Puis, on crée un crontab :

```bash
contab -e
# on rajoute
# @weekly /usr/local/sbin/isdmbckp.sh
```

## Configuration avancée

### Optimisation, tuning

Selon le nombre de fichiers à sauvegarder, ça peut valoir le coup de [modifier certains paramêtres](https://restic.readthedocs.io/en/latest/047_tuning_backup_parameters.html)

#### Resticprofile

[`resticprofile`](https://creativeprojects.github.io/resticprofile) vous permet de gérer vos configurations `restic` de façon simple dans un fichier de configuration.

> *NB : Pour ceux qui sont familiers de `borgbackup`, c'est un peu l'équivalent de `borgmatic`*

Tout ce qui a été vu précédemment est pris en charge et ce dernier permet aussi :

  - de s'assurer qu'il y a [suffisamment de RAM disponible](https://creativeprojects.github.io/resticprofile/usage/memory/index.html) pour que le backup fonctionne.
  - de modifier la priorité du processus de sauvegarde (nice)
  - de faire appel à des hooks (http / commandes), avant ou après la sauvegarde,
  - d'empêcher le système de rentrer en sommeil.
  - etc...

### Securité

Comme mentionné au départ, il peut être utile de faire tourner restic avec [un utilisateur dédié non-root](https://restic.readthedocs.io/en/stable/080_examples.html#backing-up-your-system-without-running-restic-as-root)

Pour se prémunir de problèmes liés à des ransomware, il faut utiliser un mode `append-only`; pour faire cela, sur du SFTP, il faudra passer par rclone. C'est beaucoup plus simple sur S3, par le [verrouillage des objets (WORM)](https://docs.aws.amazon.com/fr_fr/AmazonS3/latest/userguide/object-lock.html) (à associer également au versionning S3).

Quelques lectures intéressantes à ce sujet :

  - [restic avec rclone](https://ruderich.org/simon/notes/append-only-backups-with-restic-and-rclone)
  - [restic sur Hertzner avec rclone](https://fluix.one/blog/hetzner-restic-append-only/)
  - [Security considerations in append-only mode](https://restic.readthedocs.io/en/latest/060_forget.html#security-considerations-in-append-only-mode)

Avec cette méthode, attention donc à la suppression des anciens snapshots.

`rclone` est disponible sous forme de module sur le cluster Muse (installé à `/nfs/work/agap_id-bin/img/rclone/1.54.1/rclone`).

### Alerting

Maintenant, ça serait bien d'être informé si les sauvegardes se sont mal déroulées et connaître les statistiques d'utilisation disque par les snapshots.

Pour vérifer l'état d'un dépôt :

```bash
restic -r sftp:isdmbckp:workdir/restic check
```

On rajoutera donc cette commande à notre script, avec la sortie qui ira aussi vers nos logs.

#### Courriel

Le mail est l'option la plus simple pour être averti, mais il en existe de nombreuses autres (webhooks, push, etc) (voir par exemple ce [petit code python](https://github.com/remyd1/process-alert) que j'ai développé).

Il faut d'abord s'assurer que le hostname est défini correctement, sinon le serveur relais refusera de router le mail :

```bash
hostname
```

La commande hostname doit renvoyer le nom complet de la machine avec un nom de domaine valide (chez nous `*.mbb.cnrs.fr`).

Si ce n'est pas le cas :

```bash
hostnamectl hostname <bigmemX>.mbb.cnrs.fr
```

```bash
apt install -y postfix mailutils
```

On laisse les options de configuration par défaut, puis on modifie le fichier `/etc/postfix/main.cf` pour utiliser notre serveur de relais (à demander à l'équipe ISI).

```bash
sed -i "s/relayhost = /relayhost = [<fqdn.du.hostname.relais>]/" /etc/postfix/main.cf
```

Ensuite, on modifie le fichier `/etc/aliases` afin que notre adresse mail soit associée à l'utilisateur `root`, en rajoutant par exemple la ligne :

`root: moi@exemple.fr`

Puis, pour que nos changements soient pris en compte :

```bash
newaliases
postfix reload
```

Vous pouvez ensuite vérifier que l'envoi de mail est fonctionnel :

```bash
echo "test test test" |mail -s "ceci est un test" root
```

En cas de soucis, il faut vérifier la sortie de `/var/log/mail.log`.

##### Modification du script pour prise en charge de la vérification et le courriel

```bash
echo '#!/bin/bash

# nécessaire si appelé depuis un envionnement cron
export PATH=$PATH:/usr/bin/:/usr/sbin:/usr/local/bin:/usr/local/sbin

REPOSITORY="sftp:isdmbckp:workdir/restic"
DATE=$(date +"%Y%m%d")
export RESTIC_PASSWORD=<votre mot de passe>

if [ ! -d /var/log/restic/ ]; then
  mkdir /var/log/restic/
fi

LOG=/var/log/restic/${DATE}.log

for d in /bigvol/*/backup; do
  if [ -d ${d} ]; then
    restic --verbose forget --path ${d} -r ${REPOSITORY} --keep-weekly 3 --keep-monthly 3 --prune >> ${LOG}
    restic --verbose backup --tag ${DATE}_${HOSTNAME}_$(dirname ${d:8}) -r ${REPOSITORY} ${d} >> ${LOG}
  fi
done

restic stats -r ${REPOSITORY} >> ${LOG}
restic -r sftp:isdmbckp:workdir/restic check >> ${LOG}

cat ${LOG} | mail -s "Votre rapport hebdomadaire de sauvegarde pour ${HOSTNAME}" root' > /usr/local/sbin/isdmbckp.sh

chmod 750 /usr/local/sbin/isdmbckp.sh
```

A nouveau, pensez à ajuster dans le sxcript le mot de passe et l'adresse de votre dépôt.