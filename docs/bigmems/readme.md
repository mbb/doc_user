# Bigmems - README

Dans cette section, vous trouverez des informations à propos des bigmems.

> _Dernière mise à jour : 04/03/2024._

* [Présentation du service](bigmems.md)
* [Post-configuration](post-configuration.md)
* [sécurisation et mises à jour](updates_and_security.md)
* [Exemple de procédure de sauvegarde](backup.md)
