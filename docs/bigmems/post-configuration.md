# Prise en main et 1er pas administration de la bigmem 

Les machines bigmems ne sont pas administrées par le service ISI, mais uniquement monitorées avec parfois quelques modifications déployées pour que leur configuration reste homogène avec le reste. Cette documentation n'a rien d'obligatoire et vous pouvez tout à fait gérer votre système de manière totalement différente. Elle n'a pas pour but d'être exhaustive sur l'utilisation et l'administration d'une bigmem mais donne quelques clés pour une utilisation partagée.

En effet, il est considéré ici que vous avez des notions sur un système d'exploitation Linux et connaissez les commandes de base.

## Accès aux machines

La procédure d'accès à la machine a du vous être communiqué par mail.

Afin de vous connecter depuis un lieu non connu, vous pouvez utiliser le VPN de l'UM ou bien utiliser une solution interne (teleport, règle spécifique...); dans ce cas, merci de [faire un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html). La documentation Teleport est accessible [ici](https://gitlab.mbb.cnrs.fr/-/snippets/4).

## Organisation du stockage

Il est utile de bien réfléchir à l'organisation qu'on souhaite mettre en oeuvre, même si certains choix sur les volumes ou les partitions ont déjà étaient faits.

Par exemple, on peut décider que tout ce qu'on souhaitera sauvegarder sera mis dans un sous-dossier `~/backup`

> NB : **il n'y a pas de sauvegarde sur les bigmems ! C'est à vous la [mettre en place](/doc_user/site/bigmems/backup/)**.

On peut aussi définir un dépôt pour nos scripts (c'est très fortement recommandé), en utilisant la [forge gitlab MBB](https://gitlab.mbb.cnrs.fr). La connaissance de l'outil `git` est un préalable.

### Suppression des données

Sur les machines réservables, toutes les données seront supprimées au début de la réservation suivante. Si vous souhaitez un prolongement de votre réservation, pensez [à faire un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html).

Pour celles non réservables, avant le départ d'un agent, il faut impérativement voir avec lui ce qui doit être sauvegardé, déplacé pour être partagé plus amplement, archivé ultérieurement...

Ce qui doit être partagé plus largement devra être déplacé vers un endroit adéquat et les droits devront être ajustés avec les bons groupes afin de garantir des accès possibles aux collaborateurs souhaités.

Si vous manquez de place et avec l'accord de vos collaborateurs, je vous suggère également de supprimer régulièrement vos données intermédiaires, surtout si les résultats ont déjà été publiés.

Dans tous les cas, renseignez-vous auprès de votre équipe afin de connaître les systèmes de stockage éventuels supplémentaires auxquels vous avez accès.

 - [Rappel sur les droits unix](https://wizardzines.com/comics/permissions/)

Pour changer un groupe sur un dossier de manière récursive, vous pouvez utiliser `sudo chgrp -R <nouveau-groupe> /mon-dossier` ou utiliser la commande `chown`.

## Gestion d'espaces disques

L'utilitaire `ncdu` est assez pratique pour visualiser l'espace disponible sur une machine pour un dossier précis.

```bash
ncdu /
```

### ZFS

Le stockage conséquent en local est, le plus souvent, géré par ZFS avec ZFSOnLinux. Le volume système est lui géré plus classiquement en ext4.

Quelques commandes ZFS utiles :

```bash
# vérifier que le volume est dans un état correct
sudo zpool status -xv

# lister les datasets
sudo zfs list

# vérifier quelle compression est utilisée et son niveau actuel
sudo zfs get compression
sudo zfs get compressratio

# changer le point de montage du volume ZFS avec `zfs_bigvol`
# comme dataset
sudo zfs set mountpoint=/mnt/bigvol zfs_bigvol
```

> **/!\ Attention**, si vous changez le point de montage, pensez à adapter vos scripts, Homedir, etc.

Si vous avez des problèmes de montage du volume au démarrage, il faudra utiliser la commande [`sudo zpool import`](https://openzfs.github.io/openzfs-docs/man/master/8/zpool-import.8.html), mais il est préférable de [faire un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) dans ce cas.


## Configuration de la création des utilisateurs

Vous pouvez modifier le fichier `/etc/default/useradd` pour que la commande `useradd` corresponde à votre installation.

En effet, sur la plupart des machines, les volumes conséquents sont dans `/bigvol`.

```bash
sudo nano /etc/default/useradd
# on remplace /bin/sh par /bin/bash
# puis /home par /bigvol
```

La création des utilisateurs devient :

```bash
sudo useradd -m toto
```

L'utilisateur *`toto`* aura le shell `/bin/bash` et son `$HOME` sera créé dans `/bigvol`.

Si on veut ensuite lui rajouter sa clé publique, tout en mettant les bons droits :

```bash
sudo install -d -m 0700 -o toto -g toto ~toto/.ssh
sudo install -m 0600 -o toto -g toto /dev/null ~toto/.ssh/authorized_keys
# avec "ssh-<method> <contenu de la clé publique>" à remplacer
echo 'ssh-<method> <contenu de la clé publique>' |sudo tee -a ~toto/.ssh/authorized_keys
```

## Vérification des groupes et identifiants

Commandes en vrac :

```bash
# afficher les détails d'un utilisateur (de tous les utilisateurs si utilisateur non spécifié)
getent passwd [toto]
# afficher les détails d'un groupe (de tous les groupes si groupe non spécifié)
getent group [<group>]

# voir les membres du groupe sudo
getent group sudo

# afficher les groupes courant
groups
# afficher les groupes de l'utilisateur toto (le 1er est le groupe primaire)
groups [toto]

# afficher les uid / gid et noms de groupe de l'utilisateur courant
id
# idem mais sur l'utilisateur toto
id toto
```

Pour modifier temporairement son groupe primaire (à condition que le groupe soit déjà affecté) :

```bash
newgrp docker
```

Pour modifier définitivement le groupe primaire d'un utilisateur :

```bash
sudo usermod -g docker toto
```

Attention, il faudra penser à changer ensuite les droits sur les anciens dossiers ou fichiers créés précédemment par ou pour cet utilisateur (cf. commande `chown`). Il faudra également re-rajouter l'ancien groupe si besoin (voir ci-dessous).

Pour rajouter un groupe à un utilisateur existant :

```bash
sudo usermod -aG docker toto
```

## Droits par défaut

Sous Linux, les droits par défaut sont définis par la valeur `umask` :

```bash
umask
```

Un `umask` de `0022` créera des fichiers en mode `0644` et des dossiers en mode `0755`.

Pour modifier cette valeur, il suffit de rajouter une valeur d'`umask` différente dans votre fichier `~/.bashrc`.

Si vous souhaitez que cette modification se fasse sur tous les utilisateurs, il faut modifier `/etc/profile`.

Plus d'informations sur l'`umask` [ici](https://www.computernetworkingnotes.com/linux-tutorials/how-to-change-default-umask-permission-in-linux.html).

