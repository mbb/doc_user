# Bigmems

_Dernière mise à jour 03/05/2024_

## Introduction

Les bigmems désignent à l'ISEM des machines performantes qui tournent très majoritairement sous GNU/Linux, en Ubuntu LTS (`20.04` ou `22.04`). Elles vous permettent d'avoir en local des performances élevées, tout en ayant la possibilité d'être `root` sur la machine (ce qui n'est pas le cas sur un cluster de calcul), au moins le temps de la réservation sur les machines réservables. Il est donc supposé que vous êtes autonome sur un terminal Linux.

Il existe plusieurs types de bigmems :

  1. les machines privées ISEM,
  2. les machines réservables LabEx CeMEB,
  3. les machines réservables ISEM.

La grande majorité des machines bigmem réservables sont d'anciennes machines privées achetées par des équipes ou des chercheurs de l'ISEM.

Le manque de place ne nous permet pas d'acheter un nombre infini de machine, donc chaque achat fait l'objet d'une réflexion pour son installation ou non. Les nouvelles bigmems achetées doivent être mutualisées au minimum entre plusieurs chercheurs, ou mieux, entre plusieurs équipes.

Si votre besoin est conséquent sans être **très** conséquent, il vous faudra peut-être aussi (re)considérer l'achat d'une "simple" workstation bien configurée.

Les machines réservables disponibles sont réservables sous GRR, après authentification : https://mbb.univ-montp2.fr/grr

Les machines doivent être réservées **au minimum entre 24 et 48h heures plus tôt**. Chaque réservation commence à minuit et l'unité de réservation la plus petite est la journée.

Pour la durée, au plus, vous pouvez réserver une machine 6 semaines consécutives, durée renouvelable 1 fois, sur une période de 5 mois glissants. Pour les machines réservables ISEM, un peu de flexibilité sur ces durées reste possible.

## Principe général


> **Info** Sur les machines réservables, un espace large est présent dans le répertoire personnel de l'utilisateur. Il s'agit d'un aggrégat de disques sous ZFS. Autrefois situé dans `/media/bigvol/<username>`, ce dernier se situe désormais dans `/home/<username>`. `/media/bigvol` étant désormais un lien symbolique qui pointe uniquement vers `/home`. Sur les machines non réservables, cet espace conséquent se situe le plus souvent dans `/bigvol` et éventuellement `/newvol`.

### Machines réservables

Les réservations de bigmems, lorsqu'elles sont validées par l'administrateur, sont ensuite réinstallées dans la nuit de votre 1er jour de réservation. Il n'y a pas de prise en compte de l'heure exacte de début (si cette dernière est spécifiée).

Vous recevrez ensuite un mail automatique vous indiquant comment vous connecter à la machine avec un mot de passe.

Votre utilisateur a des droits élevés de sudoer, vous permettant d'installer ce que vous voulez. Ainsi, aucun support particulier n'est assuré; il est assumé que vous êtes autonome sous un shell Linux.

La machine étant réinstallée à la prochaine réservation, vous devez vous assurer d'avoir récupéré toutes vos données avant que votre réservation soit terminée. Si vous vous rendez compte que vous risquez que la durée risque d'être trop courte, merci de faire un [ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html) au plus tôt, pour éviter qu'une réservation soit validée après la votre (...), et demander une prolongation.

> **Warning** Ne faites surtout pas vous-même une nouvelle réservation derrière la votre ! En effet, si l'administrateur ne se rend pas compte que l'utilisateur est le même et de la volonté de continuer de travailler sur la même machine, il validera la nouvelle réservation et la machine sera réinstallée ! Les données seront perdues !

Pour plus de détails sur le fonctionnement technique, merci de vous reporter à la section [fonctionnement](#fonctionnement).

Les programmes installés dans `/share/apps` sur nos clusters de calcul sont également disponibles sur les bigmems, tout comme les [modules](/doc_user/site/cluster/cluster_module/#utiliser-module) et [guix](/doc_user/site/guix/). Si vous constatez un dysfonctionnement, merci également de faire [un ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html).

#### Machines réservables ISEM

Les machines réservables disponibles pour l'ISEM se font sur demande, après création d'un [ticket](https://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html).
Vous verrez alors apparaître dans [grr](https://mbb.univ-montp2.fr/grr) une nouvelle catégorie, avec vos nouvelles machines.


### Machines non réservables

Vous êtes censés être totalement autonome, néanmoins, un monitoring passif (métrologie) est effectué sur la machine pour vérifier son état.

Il n'y a **aucune sauvegarde** sur la machine, comme pour les machines réservables. ISI et MBB n'installeront pas de logiciels sur la machine, hormis ceux installés en standard ; c'est à vous de le faire. Il est conseillé d'utiliser des méthodes reproductibles pour le faire, tel que [guix](/doc_user/site/guix/) ou, à minima, "répétables", comme avec des conteneurs ou conda. Pensez aussi à versionner vos recettes, scripts ou tout autre fichier texte qui pourrait servir ultérieurement (voir [forge gitlab MBB](/doc_user/site/gitlab/)).

Une procédure de post-configuration est également disponible [ici](/doc_user/site/bigmems/post-configuration/), ainsi qu'une procédure pour [gérer la sécurité et les mises à jour](/doc_user/site/bigmems/updates_and_security/) et [un exemple de procédure de sauvegarde](/doc_user/site/bigmems/backup/).

#### Limitations

- Responsabilité : le service d'hébergement ne peut pas être poursuivi ou tenu responsable d'une perte de données, d'une machine compromise ou toute autre défaillance de la machine.

- Durée : le suivi physique de la machine est assuré jusqu'à la fin de la garantie de la machine. Au-delà de cette période, la machine est considérée comme faillible, et pouvant être retirée si nécessaire (après information à l'acheteur).


## MBBWorkflow

Le service conteneurisé (avec [_docker swarm_](https://docs.docker.com/engine/swarm/) + [_shinyproxy_](https://shinyproxy.io/)) **MBBWorkflow** disponible n'ayant guère trouvé son public, il est désormais possible de transformer une bigmem en machine "_bioco_" compatible, c'est à dire permettant de faire tourner des workflows bioinformatiques de type **MBBWorkflow**. 

Lorsque vous faite votre réservation, indiquez simplement "Oui" pour MBBWorkflow à la fin. Toute les bigmems réservables, à l'exception de la bigmem0 (pas sur le bon réseau) sont compatibles.

Il vous suffira ensuite de suivre les instructions [ici](https://gitlab.mbb.univ-montp2.fr/jlopez/subwaw) pour terminer l'installation lorsque vous aurez accès à cette dernière.

## Problèmes habituels

Si vous avez déjà utilisé une bigmem et qu'elle vous est de nouveau attribuée (ou bien vous avez choisi la même), vous aurez un message de clé SSH déjà connues par votre 
système (ou pour être plus précis de Fingerprint de l'hôte).

C'est tout à fait normal, puisque de nouvelles clés ont été régénérées à la réinstallation de la machine. Vous n'avez donc rien à craindre.

Le message commence ainsi :

```text
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
```

La solution est souvent donnée dans le message lui-même avec la commande à faire.

Si ce n'est pas le cas, vous pouvez éditer votre fichier `known_hosts` ou effectuer un ou plusieurs [`sed`](https://www.gnu.org/software/sed/manual/) selon les besoins; par exemple :

```bash
ssh remy@<ip.ip.ip.ip hidden>

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:...................................
Please contact your system administrator.
Add correct host key in /home/remy/.ssh/known_hosts to get rid of this message.
Offending ED25519 key in /home/remy/.ssh/known_hosts:296
Host key for <ip.ip.ip.ip hidden> has changed and you have requested strict checking.
Host key verification failed.

# suppression de la clé concernant cet hôte.
# Celle-ci est présente à la ligne 296 
sed -i 296d /home/remy/.ssh/known_hosts
```

## Fonctionnement réservation

<details>

<summary>Détails technique de fonctionnement interne (cliquez ici pour déplier)</summary>

Les réservations validées sont reportées dans un caldendrier système au format <a href="https://fr.wikipedia.org/wiki/CalDAV">caldav</a> grâce à un hack de <a href="https://github.com/JeromeDevome/GRR">GRR</a> et des scripts maison (voir <a href="https://github.com/JeromeDevome/GRR/issues/60#issuecomment-439018679">ici</a> pour plus d'informations). Un Cron sur un master salt vérifie alors ce calendrier. Lorsque le script lancé par cron détecte une nouvelle réservation, il vérifie le contenu de l'évènement.

<br />

Selon le type et le contenu de l'évènement, au choix, il redémarrera un conteneur GPU sur des machines GPU, réinstallera une bigmem, et rajoutera éventuellement les spécificités systèmes pour les machines <i>"bioco"</i> compatibles <b>MBBWorkflow</b>.

<br />

Le master saltstack contrôle les serveurs <a href="https://fai-project.org/">FAI</a> /tftp et DHCP. Ainsi, ces derniers sont d'abord mis à jour avec un orchestrateur <a href="https://docs.saltproject.io/en/latest/topics/orchestrate/index.html">SaltStack</a>, puis la machine est redémarrée par <a href="https://docs.saltproject.io/en/latest/">SaltStack</a>. Lorsque la machine a été réinstallée par FAI et que le paquet SaltStack a été installé, cette dernière contacte le master SaltStack qui la rajoute à nouveau puis déploie les recettes de post-install.
</details>
