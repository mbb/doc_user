# Sécurité et mises à jour

## Mises à jour

Afin de maintenir une machine, que ça soit pour vos logiciels ou la sécurité de celle-ci, il convient de faire régulièrement les mises à jour.

Le système `Ubuntu` fournit une nouvelle distribution majeure de type [`LTS`](https://doc.ubuntu-fr.org/lts) tous les 2 ans, au mois d'avril. Ces dernières sont maintenues pendant 5 ans.

Si les mises à jour des packages et du noyau sont très importantes, il est possible de retarder un peu le déploiement d'une `LTS` afin de ne pas essuyer les premiers problèmes. Néanmoins, il ne faut non plus trop tarder, car plus on tarde à déployer un eversion majeure, plus l'opération devient risquée au fil du temps. On peut par exemple considérer qu'avoir 6 mois ou un an de retard sur le déploiement d'une distribution majeure ne sera guère problématique et qu'attendre plus ou beaucoup plus que 2 ans pourra éventuellement poser problème.

### Unattended-upgrade

Si vous souhaitez éviter de faire régulièrement :

```bash
apt update
apt list --upgradable
apt upgrade -y
```

Une première solution consiste à [installer et utiliser `apticron`](https://www.cyberciti.biz/faq/apt-get-apticron-send-email-upgrades-available/) afin d'être informé qu'il n'y a pas de nouvelle mise à jour disponible (de manière quotidienne).

Une autre possibilité, sur système `apt`, est d'utiliser `Unattended-upgrade` (peut également être combiné à `apticron`).


### Configuration

Un gros avantage de `Unattended-upgrade` est de pouvoir les mises à jour tout seul sans aucune intervention. 

*Problème* : `apt` peut parfois poser des questions pour savoir s'il doit mettre à jour un fichier de configuration ou non, et peut aussi poser des questions sur la résolution de problèmes.

`Unattended-upgrade`, par défaut, garde l'ancienne version (les commentaires commencent par `//`) :

```
// This option allows you to control if on a unclean dpkg exit
// unattended-upgrades will automatically run 
//   dpkg --force-confold --configure -a
// The default is true, to ensure updates keep getting installed
//Unattended-Upgrade::AutoFixInterruptedDpkg "true";
```

Il est évidemment possible d'éviter la mise à jour de certains paquets :

```
// Python regular expressions, matching packages to exclude from upgrading
Unattended-Upgrade::Package-Blacklist {
    // The following matches all packages starting with linux-
//  "linux-";

    // Use $ to explicitely define the end of a package name. Without
    // the $, "libc6" would match all of them.
//  "libc6$";
//  "libc6-dev$";
//  "libc6-i686$";

    // Special characters need escaping
//  "libstdc\+\+6$";

    // The following matches packages like xen-system-amd64, xen-utils-4.1,
    // xenstore-utils and libxenstore3.0
//  "(lib)?xen(store)?";

    // For more information about Python regular expressions, see
    // https://docs.python.org/3/howto/regex.html
};
```

Il peut être utile de figer la `libc` ou une version de python pour vos besoins.

Pour ne faire que les mises à jour de sécurité :

```
Unattended-Upgrade::Allowed-Origins {
    "${distro_id}:${distro_codename}";
    "${distro_id}:${distro_codename}-security";
    // Extended Security Maintenance; doesn't necessarily exist for
    // every release and this system may not have it installed, but if
    // available, the policy for updates is such that unattended-upgrades
    // should also install from here by default.
    "${distro_id}ESMApps:${distro_codename}-apps-security";
    "${distro_id}ESM:${distro_codename}-infra-security";
//  "${distro_id}:${distro_codename}-updates";
//  "${distro_id}:${distro_codename}-proposed";
//  "${distro_id}:${distro_codename}-backports";
};
```

C'est ce que je fais sur la plupart de nos machines, mais vous pouvez l'étendre aux mises à jour classiques en décommentant `"${distro_id}:${distro_codename}-updates";`.

#### courriel

Si vous avez suivi [la procédure de configuration mail dans l'article sauvegarde](/doc_user/site/bigmems/backup/#courriel), il ne vous reste plus qu'à mettre votre courriel dans la configuration d'`unattended-upgrades` :

```
//Unattended-Upgrade::Mail "";
```

Il ne vous reste plus qu'à redémarrer le service :

```bash
systemctl restart unattended-upgrades.service 
```

## SSH

La configuration SSH peut être durcie de plusieurs manières. La plus importante est certainement d'éviter les connexions en Users/password en particulier pour l'utilisateur `root`, et de limiter le nombre de tentatives...

RedHat a produit [un guide opur sécurisé SSH](https://www.redhat.com/sysadmin/eight-ways-secure-ssh). Dans celui-ci vous trouverez notamment :

```
PermitRootLogin no
PermitEmptyPasswords no
```

Pour éviter toute connexion par utilisateur / mot de passe, il faut rajouter :

```
PasswordAuthentication no
```

Cela implique que tous les utilisateurs devront avoir généré et copié leur clé en aval sur la machine, avant la prise en compte de la modification, ou donner une copie de leur clé publique à l'administrateur du système (qui sera toujours connecté avant la prise en compte effective de la modification) :

```bash
ssh-keygen -t ed25519
ssh-copy-id <user>@<bigmem>
```

Les prochaines connexions devraient ainsi éviter l'étape du mot de passe.

D'autres techniques peuvent être utiles, comme le port knocking, le changement de port, choisir des protocoles de chiffrement réputés plus sûrs, etc.

[Voir ici pour d'autres méthodes](https://www.golinuxcloud.com/prevent-brute-force-ssh-attacks-centos-linux/), ou bien [ce guide sur OpenSSH par l'équipe Infosec de Mozilla](https://infosec.mozilla.org/guidelines/openssh.html).

Personnellement, je rajoute [`Fail2ban`](https://www.it-connect.fr/premiers-pas-avec-fail2ban/) ou bien [`crowdsec`](https://www.it-connect.fr/comment-proteger-son-serveur-linux-des-attaques-avec-crowdsec/). Sur le cloud, je préfère utiliser le second, qui me semble plus pertinent pour ce cas d'usage.

## Pour aller plus loin

Une bonne règle en sécurité est de toujours donner le moins d'accès possible sans empêcher pour autant de travailler. Nous pouvons donc éditer les droits, les accès et les groupes par utilisateur afin de satisfaire cette règle.

Avant d'installer des programmes, il faudrait analyser ces derniers afin de vérifier que rien de met en danger votre système. par exemple, dans un dépôt github, vous pouvez faire des recherches sur des expressions comme `http` pour voir les appels Web. Normalement, il ne faut rien installer en dehors de sources de confiance.

Si vous installer un conteneur docker, pensez à le scanner avec l'outil [`trivy`](https://github.com/aquasecurity/trivy). Ce scanner vous listera les vulnérabilités connues sur une image.

### Quelques outils supplémentaires

Pour scanner régulièrement votre machine, le plus classique est d'installer les outils [`clamav`](https://www.malekal.com/comment-installer-et-utiliser-clamav-sur-linux/) / [`rkhunter`](https://doc.ubuntu-fr.org/rkhunter) / [`chkrootkit`](https://debian-facile.org/doc:autres:chkrootkit). Vous pouvez aussi installer [`Lynis`](https://www.it-connect.fr/scan-de-votre-systeme-unix-avec-lynis/) afin d'avoir plus d'informations d'audit.