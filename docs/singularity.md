<!-- toc -->

# Utilisation de Singularity

[Singularity](https://sylabs.io/singularity/) [^1](#note-1) va nous permettre de faire tourner des logiciels sur le cluster à partir de conteneurs. Ces conteneurs peuvent être importés depuis docker ou fabriqués directement avec singularity. Un conteneur est une sorte de machine virtuelle très allégée qui a un isolement plus faible.

Singularity offre plusieurs avantages :
  
  - un simple binaire exécutable
  - pas de démon
  - pas besoin d'être `root` ou `sudoer` ou pour lancer le conteneur.

[![Alt text](https://img.youtube.com/vi/dz136-GPUSo/0.jpg)](https://www.youtube.com/watch?v=dz136-GPUSo)

La construction de l'image du conteneur doit se faire sur votre machine personnelle ou dans une machine virtuelle dans laquelle vous avez des droits `sudo`/`root`. En effet, l'étape de `build` nécessite des droits élevés. Il faut donc au préalable [installer singularity](https://www.sylabs.io/guides/3.4/user-guide/installation.html) (ou en [v2.6](https://www.sylabs.io/guides/2.6/user-guide/installation.html) pour *meso@lr* par exemple) sur votre machine ou VM. Il est fortement conseillé d'utiliser la même version de Singularity sur votre machine que sur le cluster cible (vous pouvez vérifier les versions de singularity disponibles avec la commande `module av`).
Le `build` va se faire à partir d'une recette. Il est également possible de faire des `pull` (sorte de téléchargement) directement et d'utiliser directement ces conteneurs (hébergés sur des registres d'image comme le *singularity-hub* ou *DockerHub*...).

Les recettes Singularity sont composés d'un en-tête puis de sections :

Les contenu des sections s'exprime en bash, dans des _"sections"_.

Exemple ([depuis la documentation Singularity](https://www.sylabs.io/guides/2.6/user-guide/build_a_container.html#building-containers-from-singularity-recipe-files)) :

```
Bootstrap: docker
From: ubuntu:16.04


%post
    apt-get -y update
    apt-get -y install fortune cowsay lolcat


%environment
    export LC_ALL=C
    export PATH=/usr/games:$PATH


%runscript
    fortune | cowsay | lolcat
```

Construction de l'image depuis la recette :

```bash
$ sudo singularity build lolcow.simg Singularity
```

[Plus d'informations sur les sections, recettes](https://www.sylabs.io/guides/2.6/user-guide/container_recipes.html#container-recipes).

Pour lancer ce conteneur, il suffira de faire :

```bash
singularity run lolcow.simg
```

Mais il est possible, depuis l'hôte, de lancer n'importe quel programme présent dans le conteneur :

```bash
# ici le programme "cat"
singularity exec lolcow.img cat /etc/os-release
```

Lorsque vous avez fini la construction de votre image, copiez la sur le cluster puis essayez de l'exécuter ou de faire un `singularity shell` dedans.

Si vous avez besoin d'accéder à un dossier de l'hôte, en dehors de votre `$HOME` et du `/tmp`, vous pouvez monter ce dossier (on parle de `bind mount`) dans votre conteneur, avec l'option `-B` :

```bash
cd /tmp
singularity shell -B /opt/gridengine:/opt/gridengine ubuntu_16.04.sif
> Singularity ubuntu_16.04.sif:/tmp> ls -l /opt/gridengine
> ...
```

L'équipe MBB met à votre disposition 2 applications pour générer des recettes :
  - une application Shiny spécialisée pour des applications bioinformatiques : [RPACIB](https://shiny.mbb.cnrs.fr/RPACIB/),
  - une application Web plus générique : [WICOPA](https://web.mbb.cnrs.fr/wicopa/) ([code opensource, license Apache 2.0](https://gitlab.mbb.cnrs.fr/jlopez/wicopa)).

Certaines images sont déjà disponibles dans `/share/apps/sing-images/$SINGULARITY_VERSION`. Si vous pensez que votre image peut être utile à d'autres personnes et que vous souhaitez la partager, n'hésitez pas à nous en faire part et nous la rajouterons !

L'exécution sur le cluster est aisée, le $HOME et le TMP sont montés par défaut dans votre conteneur et l'utilisateur dans le conteneur est le même que sur l'hôte.
Un `modulefiles` vous permet de charger singularity (voir [utilisation de module](cluster.md#utiliser-module)).

Un petit exemple basique d'un fichier de soumission SGE/singularity serait (après avoir chargé le module singularity) :

```bash
#$ -S /bin/bash
#$ -N Singularity_Job
#$ -q cemeb.q
#$ -l h_rt=10:00:00
#$ -cwd
#$ -V

singularity exec myfirstimage.simg myprog --myargs > myoutputfile.out
```

# Modifier une image

Souvent, pour des recettes compliquées, vous ne serez pas satisfait du résultat, ou le `build` va échoué.

Dans ce cas, il faudra vérifier à quelle ligne ça ne fonctionne pas, ou mieux, repartir de l'image de base (`From: <image>`) et se mettre dans un shell.

Nous ferons alors nos tests dans un bac à sable `sandbox`, puis lorsque notre image est fonctionnelle, notre historique de commandes (commande `history`) nous permettra ensuite d'écrire une nouvelle recette propre.

Exemple :

```bash
singularity pull ubuntu:18.04
sudo singularity build --sandbox ubuntu18 ubuntu_18.04.sif
sudo singularity shell -w ubuntu18
# on fait les opérations qui ont échoué dans la section %post
# afin de les tester une à une et éditer la recette le cas échéant
# pour avoir une image fonctionnelle
```

Lorsque la recette est modifiée :

```bash
sudo singularity build <image.sif> <recette.def>
# on transfère ensuite l'image sur le cluster
scp <image.sif> <login@adresse>:
```

# TP singularity avance

Voici un TP assez avancé que j'avais fait dans le cadre de l'[ANF UST4HPC](https://ust4hpc.sciencesconf.org/), en version `2.4.6` :
https://mbb.univ-montp2.fr/ust4hpc/tpSingu.html

# Rajouts classiques specifiques au Cluster BlueBanquise ISEM

De manière générale, je vous conseille de rajouter le contenu suivant à votre section `%post` :

```bash
  # Specific Cluster config
  mkdir -p /share/apps/bin
  mkdir /share/apps/lib
  mkdir /share/bio
  mkdir -p /export/scrach
  mkdir -p /usr/lib64
  ln -s /bin/bash /bin/isem_bash
```

En effet, par défaut, je rajoute automatiquement (dans la configuration générale de _singularity.conf_), les _bind mount_ suivant (en plus du `/tmp` et du `$HOME`):

```
bind path = /share/apps/bin
bind path = /share/apps/lib
bind path = /export/scrach
```

## OpenMPI

> Note: la version 1.6.2 n'est plus disponible depuis la mise à jour du cluster.

Par ailleurs, si vous souhaitez utiliser MPI, il faudra utiliser la même version que celle sur le cluster (toujours en `%post`) :

```bash  
  cd /usr/local
  ## MPI stuffs (installing the same OpenMPI version that the one on the nodes)
  VERSION=2.1.1
  wget -O /tmp/openmpi-${VERSION}.tar.bz2 https://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-${VERSION}.tar.bz2
  tar -xf /tmp/openmpi-${VERSION}.tar.bz2
  cd openmpi-${VERSION}
  ./configure --prefix=/usr/local --enable-static
  make -j4
  make install
```

# Utilisation sur le cluster meso at lr

  *Merci à Rémi Allio pour ses tests et ses notes*

Comme dans beaucoup de cluster, vous ne disposez pas de droit élevés sur le cluster Meso@LR.
Sous ces conditions, la création d’une image singularity peut s’avérer cruciale.

Comme expliqué précédemment, pour créer cette image vous nécessitez des droits administrateur et vous devez donc la construire depuis votre machine. 

Quelques petits conseils pour le cluster *Meso@LR* :

Le cluster monte de nombreux dossiers par nfs et ainsi, lorsque vous soumettez un job sur un nœud (via un fichier (s)batch par example), le chemin d’accès à vos fichiers est changé. Ainsi, au lieu d’être `/home/$USER/`, le vrai chemin sera `/nfs/home/$USER/`.

Ainsi, lors de la création de votre recette (spécifiquement pour *Meso@LR*) il sera utile d’ajouter la ligne suivante dans la section `%post` :

```bash
# remplacer $USER par votre username sur le cluster
mkdir -p /nfs/home/$USER/
```

Cette commande vous permet de créer un chemin dans votre container qui est équivalent à celui du cluster. Ensuite, lors du chargement de singularity sur votre nœud de calcul, il faudra lier le chemin du nœud au chemin du container. Pour cela il faudra effectuer la commande suivante :

```bash
singularity exec -B /nfs/home/$USER/:/nfs/home/$USER/ mon_image.img
```

De cette manière, les fichiers dans le container auront les mêmes chemins que dans le nœud de calcul (i.e. même chemin que dans votre home avec _/nfs/_ en plus).

Pour finir, par défaut les dossiers  `$HOME`, `/tmp`, `/proc`, `/sys`, `/dev` sont montés lors du chargement de singularity. Cependant, lorsque singularity est chargé depuis un nœud de calcul, le `$HOME` considéré est `/home/$USER/`. Malheureusement, depuis le nœud de calcul, vous ne disposez pas des droits sur ce HOME depuis le nœud de calcul.
L’erreur suivante va donc stopper votre job :

```
ERROR  : Home directory is not owned by calling user: /home/$USER
```

Pour pallier cette erreur, lors du chargement de singularity dans le nœud de calcul, il faut préciser le chemin du HOME à considerer comme ceci :

```bash
singularity exec --home /nfs/home/$USER:/home/$USER  -B /nfs/home/$USER/:/nfs/home/$USER/  mon_image.img
```

En effet, `/home/$USER` sur le noeud est un lien symbolique et nécessite l'existence du dossier cible `/nfs/home/$USER`, contenant vos fichiers, à créer et à monter dans le conteneur.

# Ressources

- [Sources Github de Singularity](https://github.com/sylabs/singularity),
- [La documentation officielle chez Sylabs](https://www.sylabs.io/docs/),
- [La publication Singularity](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0177459),
- Quelques registres : le [Singularity-Hub](https://singularity-hub.org/), [DockerHub](https://hub.docker.com/), [quay.io](https://quay.io/search)
- Présentation de Cédric Clerget (désormais ingénieur chez Sylabs) [Singularity, un conteneur pour le HPC](https://groupes.renater.fr/wiki/include-besancon/_media/rencontres/clerget_pres_singularity_cclerget.pdf),
- [Singularity au NIH](https://hpc.nih.gov/apps/singularity.html),
- [Ma présentation aux JDEVs 2017](http://devlog.cnrs.fr/_media/jdev2017/dev2017_p8_rdernat_short.pdf?id=jdev2017) et [celle à la journée organisée par le groupe SIL/COMET du CNES en 2018](http://kimura.univ-montp2.fr/remydernat/presentations/2018/comet/hpc_docker_singularity.pdf),
- [Nos recettes partagées sur forge gitlab](https://gitlab.mbb.cnrs.fr/mbb/singularity-local-recipes)


---


#### note ^1

Singularity est devenu [`SingularityCE`](https://sylabs.io/singularity/) d'un côté, [`apptainer`](http://apptainer.org/) de l'autre. Même si les mainteneurs et développeurs affirment le contraire, il reste probable que la compatibilité ne soit plus assurée entre les 2 versions à l'avenir.

