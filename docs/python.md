<!-- toc -->

# Python

## Introduction

> **Info** Sur les précédents clusters, il fallait charger python par `module`. Ce n'est plus nécessaire.

## Versions

Les versions installées sur le système sont les suivantes : `python2.7`, `python3.6`, `python3.8`, `python3.9`.

Par défaut, `python2` pointe vers `python2.7` et `python3` vers `python3.6`.

Vous pouvez aussi installer `python3.8.2` par guix :

```bash
guix install python@3.8.2
```

> **Warning** Pour un script de soumission, n'oubliez pas d'insérer un [`shebang`](https://fr.wikipedia.org/wiki/Shebang) avant de lancer votre code en python. Parfois l'entête du fichier python appelle directement une version de python comme par exemple `#!/usr/bin/python2` qui chargera la version `2.7` par défaut. Dans ce cas, il vaut mieux modifier cet entête (ou ), en `#!/usr/bin/env python2.7`.

## Installer un module Python

En python, vous pouvez télécharger les sources d'un package et lancer :

```bash
python setup.py install --user
```

Ou utiliser pip :

```bash
# si python2.7
python2.7 -m pip install --user monmodule
# si python3
python3 -m pip install --user monmodule
```

## Utiliser une autre version de python avec pyenv

Si les versions disponiblesdepuis le système ne suffisent pas, vous pouvez voir comment utiliser [pyenv](http://amaral-lab.org/resources/guides/pyenv-tutorial)

Ce dernier se combine bien avec [virtualenv](https://github.com/pyenv/pyenv-virtualenv).

## Installer conda

Il faut utiliser `pyenv` ci-dessus. Merci de passer l'étape `installing pyenv` si c'est déjà fait (idem pour les `virtualenvs`)...

Les installations par pyenv peuvent être assez longues, mais vous ne dépendrez plus de la version système et pourrez choisir précisemment la version que vous souhaitez.

> **Note** Conda peut aussi être installé avec [`guix`](guix.md) !

```bash
module add openssl/3.0.0 curl/7.68.0-dev

# installing pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

source ~/.bashrc

# installing virtualenv
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc

source ~/.bashrc

# pour lister toutes les versions de conda disponibles
pyenv install conda

# on en choisit une
pyenv install miniconda3-3.9.1
# si vous avez une erreur 403/Forbidden, merci d'essayer une autre version
pyenv local miniconda3-3.9.1

conda --version
> conda 4.3.30

# pour utiliser un virtualenv avec conda il faut conda >= 4.6
# cf. https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html
pyenv install miniconda3-4.3.30
pyenv local miniconda3-4.3.30
conda --version
> conda 4.6.14
conda create --name conda_venv4.6
conda activate conda_venv4.6

# on desactive le tout pour revenir à la version système de python
conda deactivate conda_venv4.6
pyenv local system
```

> **Note** Vous pouvez aussi utiliser virtualenv avec pyenv au lieu de conda (voir ex. ci-dessous avec `snakemake`

## Installer snakemake

Il faut utiliser `pyenv` ci-dessus. Merci de passer l'étape `installing pyenv` si c'est déjà fait (idem pour les `virtualenvs`)...

```bash
module add openssl/3.0.0 curl/7.68.0-dev

# installing pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

. ~/.bashrc

# installing virtualenv
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc

. ~/.bashrc

pyenv install 3.7.14
pyenv local 3.7.14

pyenv version
3.7.14 (set by ~/.python-version)
which pip
> ~/.pyenv/shims/pip
python3.7 -m pip --version
> pip 10.0.1 from ~/.pyenv/versions/3.6.7/lib/python3.6/site-packages/pip (python 3.6)

# creating the virtualenv from our current python version
pyenv virtualenv venv3.7.14
pyenv activate venv3.7.14

python3.7 -m pip install snakemake
# snakemake does not seem to be found in PATH
echo $PATH
ln -s ~/.pyenv/versions/3.7.14/envs/venv3.7.14/bin/snakemake ~/.pyenv/bin/snakemake
snakemake --help

# ....

# pour quitter le virtualenv
pyenv deactivate
# pour revenir à un version système de python
pyenv local system
```

## Installer un package avec dependances

Normalement, `conda` et `pyenv` + `virtualenv` (voir installations ci-dessus) permettent de s'en sortir. Si ce n'est pas suffisant, merci de vous reporter à la section sur les [conteneurs singularity](singularity.md). En effet, nous ne pouvons pas surcharger les disques de tous les noeuds en installations nombreuses et parfois exotiques (avec parfois des problèmes de stabilité, de non compatibilité entre packages ou de dépendances croisées)...
