<!-- toc -->

# Je ne sais pas quel service utiliser

Merci de lire [cet article](cluster/cluster_intro.md#comparaison-avec-les-autres-services-disponibles).

# Comment nous citer

Pour tout travail impliquant les services MBB, merci d'inclure cette phrase dans vos remerciements :

```text
"[Replace_with_your_project_name] benefited from the Montpellier Bioinformatics Biodiversity platform supported by the LabEx CeMEB, an ANR "Investissements d'avenir" program (ANR-10-LABX-04-01)."
```

Si le travail n'a bénéficié que d'un support purement ISEM fourni par ISI, tel que le Cluster ISEM, la citation devient :

```text
"This work benefited from the ISEM computing facility, at ISI service."
```

## Je n'arrive pas à me connecter à rstudio malgré mon login cluster

Rstudio nécessite une validation supplémentaire sur le serveur rstudio et donc doit faire l'objet d'une demande supplémentaire par l'interface de ticket.

## J'arrive à me connecter à MBBWorflow mais je ne peux pas utiliser mes données

MBBWorkflow doit faire l'objet d'une demande dédiée afin que vous ayez un `$HOME` visible sur les conteneurs.

## Fonctionnement du scratch et du home sur nos clusters

Le **scratch** sur nos clusters fonctionne souvent différemment que sur les autres clusters. En effet, ce dernier est local à chaque nœud. Si vous voulez l'utiliser, il faut donc prévoir une copie des données dans votre job vers le dossier `/export/scrach` puis les récupérer à la fin de votre job. Par conséquent, il est impossible d'utiliser un scratch avec un job de type MPI. La raison principale de ce choix est que la plupart des calculs ne sont pas du type MPI. Par conséquent, l'infrastructure est de type _[HTC](https://en.wikipedia.org/wiki/High-throughput_computing)_, et non _[HPC](https://en.wikipedia.org/wiki/Supercomputer)_, c'est à dire, sans réseau à faible latence. En l'absence d'un réseau de ce type, un filesystem parallélisé est impossible à mettre en place (à cause de performances très dégradées). En revanche pour des jobs simples, vous bénéficierez d'un accès particulièrement rapide vers ce scratch, puisque ce dernier est local à la machine.

Le **HOME** est exporté par NFS depuis quelques NAS. Ce dernier est donc le même sur tous les nœuds.

## Exemple de script SLURM avec utilisation de scratch

Comment remplacer l'usage de NFS par un accès aux disques locaux ? Il faut s'assurer que vous avez un rép. à votre nom sous `/scratch/` dans tous les noeuds de calcul.

```bash
#!/bin/bash
#SBATCH -e scratch_test."%j".err
#SBATCH -o scratch_test."%j".out
#SBATCH -t 15-00:00:00

# on met la langue en anglais afin d'avoir des messages d'erreurs connus
LANG=en_US.utf8
if [ ! -d /export/scrach/${USER} ];then 
  mkdir -p /export/scrach/${USER}
fi
SCRDIR=`mktemp --tmpdir=/export/scrach --directory ${USER}/job.${SLURM_JOB_ID}.XXXXXXX`
# on lance un code qui va écrire dans ce dossier scratch temporaire
# ici on génère une chaine de caractères aléatoires
openssl rand -base64 12 > ${SCRDIR}/job_output
cp -rp ${SCRDIR} /home/${USER}
rm -rf ${SCRDIR}
```

```bash
sbatch scratch_test.sbatch
```

# Transferts de données

## Comment transférer des données sur le cluster

Voir [cet article](tutombb.md#au-travers-d-un-accès-au-cluster-en-ssh). Pour les gros besoins/transferts, merci de [nous consulter](http://kimura.univ-montp2.fr/calcul/helpdesk_NewTicket.html). En effet, le noeud maître ou noeud de login ne doivent pas être monopolisés par des transferts qui seraient bloquants pour les autres utilisateurs.

Si besoin, [voir aussi](/doc_user/site/tips_and_bugs/index.html#transfert-de-fichier-avec-croc).

## J'ai transféré des données texte éditées sous Windows

Il faudra les transformer avec l'outil `dos2unix`. Vous pouvez vérifier que votre fichier ne contient pas de caratères spéciaux avec `cat` :

```bash
cat -vt mon_fichier.txt
# si vous voyez des caratères tels que "^M" en fin de ligne, il faudra convertir le fichier.
```

> **Hint** Pour **MacOS**, vous avez également le programme `mac2unix` contenu dans la package `dos2unix` qui fera la même chose !

## Soucis FileZilla et Fail2ban

Nos serveurs sont équipés du logiciel `Fail2ban` pour éviter des tentatives de piratage depuis l'extérieur. Cela peut poser des problèmes au logiciel `FileZilla` qui tente de muliplier le nombre de connexions / threads. En effet, votre machine sera vue comme une machine faisant des tentatives d'attaque de type [DDoS](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service) par nos serveurs.

Afin de contourner le problème, vous pouvez soit réduire le [nombre de threads simultanés à 2 dans FileZilla](https://portal.obtrix.net/knowledgebase.php?action=displayarticle&id=121), soit nous faire un ticket pour mettre [votre adresse IP publique](https://www.whatismyip.com) en liste blanche.

# Utiliser R avec SLURM

Exemple de code R (somme.R):

```r
args<-commandArgs(TRUE);
debut = as.numeric(args[1]);
fin = as.numeric(args[2]);
vect = seq(debut, fin);
cat(sum(vect),"\n");
```

Exemple de script SLURM (`submit_somme.sbatch`):

```bash
#!/bin/bash
#SBATCH --job-name="R somme"
#### si on ne précise pas de sortie erreur, la sortie 
#### standard et la sortie erreur iront dans le même 
#### fichier de sortie
#SBATCH -o submit_somme.o       
#SBATCH -t 5:0:0                #### 5 heures demandées

version="3.5.3"
module load R/$version
R CMD BATCH --slave "--args $1 $2" somme.R sortie.txt
```

>**Info** voir comment [utiliser module](cluster/cluster_module.md)

Lancer avec SLURM pour faire la somme des entiers de 1 à 10 :

```bash
sbatch submit_somme.sge 1 10
squeue
# on dort tant qu'on a pas le fichier puis on le lit
until cat sortie.txt; do sleep 3; done
```

# Connaître son quota

Il vous est possible de connaître votre quota LDAP avec le script `get_myquota.sh`. La valeur retournée n'est pas forcément celle présente sur le système. Contactez votre administrateur système en cas de doute.

# Utiliser module

Voir ici : [utiliser module](cluster/cluster_module.md)

# Comment lancer un job en réservant de la mémoire pour son job

```bash
# pour réserver 15Go de RAM par ex.
sbatch --mem=15G <my_submit_file.sbatch>
# par coeur (sinon écrasé par valeur par défaut)
sbatch --mem-per-cpu=4G <my_submit_file.sbatch>
```

> **Tag** En cas de job OpenMP, il peut être utile de rajouter `--exclusive`; à utiliser avec parcimonie !
> Pour plus d'informations, voir [ici](https://slurm.schedmd.com/cons_res.html)
