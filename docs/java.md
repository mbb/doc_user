<!-- toc -->

# Module

>**Warning** Depuis l'été 2015, le cluster MBB et celui de l'ISE-M sont passés sous module pour gérer java (voir [lien](cluster.md#utiliser-module)). Il faut donc penser à précéder votre ligne de commande par ```module load python...``` dans votre script de soumission SGE (points de suspension correspondants à la version désirée).

# Paramètres java

Voir la [documentation oracle](https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/jrdocs/refman/optionX.html) à ce sujet, en particulier les options Xms (minimum) et Xmx (maximum) pour les réglages de la mémoire réservée.

# Environnement java

[JAVA_HOME](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)

Voir aussi:

- [https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html](https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html)
- [https://docs.oracle.com/cd/E29585_01/DeveloperStudio.61x/devStudioHelp/src/cds_overriding_java_home_and_class_path_settings.html](https://docs.oracle.com/cd/E29585_01/DeveloperStudio.61x/devStudioHelp/src/cds_overriding_java_home_and_class_path_settings.html)

```bash
remy@cluster-mbb:~$ java -version
java version "1.7.0_51"
Java(TM) SE Runtime Environment (build 1.7.0_51-b13)
Java HotSpot(TM) 64-Bit Server VM (build 24.51-b03, mixed mode)
remy@cluster-mbb:~$ echo $JAVA_HOME
/usr/java/latest
remy@cluster-mbb:~$ module load java1.8
remy@cluster-mbb:~$ echo $JAVA_HOME
/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.51-3.b16.el6_7.x86_64/jre/
remy@cluster-mbb:~$ java -version
openjdk version "1.8.0_51"
OpenJDK Runtime Environment (build 1.8.0_51-b16)
OpenJDK 64-Bit Server VM (build 25.51-b03, mixed mode)
```
