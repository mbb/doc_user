# Gitlab

Le service [Gitlab MBB](https://gitlab.mbb.cnrs.fr) est un service d'hébergement de code sous le gestionnaire de version Git.


## Accès et permissions

Ce service est disponible pour tout membre de l'ISEM, du [LabEx CeMEB](https://labex-cemeb.org/) et tous leurs collaborateurs qui souhaitent participer.
  
* [Pour demander un compte](https://zammad.isi.isem-evolution.fr/)


**A noter :**

  1. Si la personne a un identifiant MBB elle pourra se connectera avec un login LDAP.
  2. Pour les membres de l'ISEM, il est possible de se connecter sur gitlab avec la solution `ISEM SSO` en-dessous du bouton de connexion. Dans ce cas, il faudra attendre la validation du compte par un administrateur ([ref. ¹](#1)).
  3. Enfin, les utilisateurs externes doivent se connecter avec l'onglet standard sur la page d'accueil (il s'agit alors d'un compte local).


<a name="1">ref. 1</a> Attention, les utilisateurs ayant déjà un compte MBB ne pourront pas utiliser cette méthode.

> Les utilisateurs déjà existants peuvent également demander à être rajouté à un dépôt spécial permettant de <u>rajouter eux-même directement des utilisateurs</u> locaux. Dans ce cas, il faut aussi au préalable demander à un administrateur l'accès à ce dépôt spécifique (le détail de fonctionnement de ce dépôt est expliqué [ici](https://remyd1.fr/comm/blog/2023/useradd-gitlab-ci/)).

Par défaut, tous les codes sont privés et nécessitent une action du propriétaire pour le rendre public. Si vous avez un compte, mais n'avez pas accès au dépôt, vous devez demander au propriétaire de celui-ci de vous y donner accès.

Les codes sont disponibles par *https* et *git+ssh*. Vous pouvez rajouter votre clé SSH en cliquant sur votre compte (icône utilisateur) \> `modifier le profil` \> `Clés SSH`.

> _Voir aussi :  [générer une clé SSH](https://www.malekal.com/generer-et-se-connecter-en-ssh-avec-des-cles-ssh/)_

## Fonctionnalités


Liste non exhaustive des fonctionnalités non actives :

* mattermost (activé un temps puis supprimé par manque d'activité sur celui-ci),
* gitlab pages.

En revanche, le gitlab-ci et le registry sont actifs.

> Pour information, l'instance est archivée de temps en temps par [SoftwareHeritage](https://archive.softwareheritage.org/browse/search/?q=gitlab.mbb&with_visit=true&with_content=true). Cela concerne les dépôts visibles de l'extérieur (publics).


## Documentation

Pour une explication de GIT et Gitlab, vous pouvez vous référer à [cette documentation](https://docs.bluekeys.org/guide/linux/git).
