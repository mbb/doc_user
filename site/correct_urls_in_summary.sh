#!/bin/bash

# modifying href from https://gitlab.mbb.univ-montp2.fr/docs/doc_user/memos/_book/cloud/
# to https://gitlab.mbb.univ-montp2.fr/docs/doc_user/memos/_book/cloud/index.html

echo
echo "Processing SUMMARY.md/index.html to fix URLs"
echo

# getting list of bad URLs with find . -name "*.html" -exec grep -rE "<a href=\".+/\">" {} \;

find . -name "*.html" -exec sed -ri "s@<a href=\"(.+)/\">@<a href=\"\1/index.html\">@g" {} \;
